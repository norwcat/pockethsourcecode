module.exports = class InvalidJwtError extends Error {
  constructor() {
    super();
    this.name = 'InvalidJwtError';
    this.message = 'JSON web token invalid';
    this.status = 400;
  }
};
