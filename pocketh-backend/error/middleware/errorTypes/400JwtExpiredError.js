module.exports = class JwtExpiredError extends Error {
  constructor() {
    super();
    this.name = 'JwtExpiredError(';
    this.message = 'JSON web token expired';
    this.status = 400;
  }
};
