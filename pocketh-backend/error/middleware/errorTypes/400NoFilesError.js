module.exports = class JwtExpiredError extends Error {
  constructor() {
    super();
    this.name = 'NoFilesError';
    this.message = 'No files found';
    this.status = 400;
  }
};
