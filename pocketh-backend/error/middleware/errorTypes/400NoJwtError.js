module.exports = class NoJwtError extends Error {
  constructor() {
    super();
    this.name = 'NoJwtError';
    this.message = 'No JSON web token found';
    this.status = 400;
  }
};
