module.exports = class NotAUserError extends Error {
  constructor() {
    super();
    this.name = 'NotAUserError';
    this.message = 'Not a logged in user';
    this.status = 400;
  }
};
