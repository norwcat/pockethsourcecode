module.exports = class EmailAlreadyExistsError extends Error {
  constructor() {
    super();
    this.name = 'EmailAlreadyExistsError';
    this.message = 'Email already exists';
    this.status = 400;
  }
};
