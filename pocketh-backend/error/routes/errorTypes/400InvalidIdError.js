module.exports = class InvalidIdError extends Error {
  constructor() {
    super();
    this.name = 'InvalidIdError';
    this.message = 'Invalid id';
    this.status = 400;
  }
};
