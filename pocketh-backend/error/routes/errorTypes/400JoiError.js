module.exports = class WrongIdFormatError extends Error {
  constructor(JoiError) {
    super(JoiError);
    this.name = 'InvalidInputError';
    this.message = JoiError.details[0].message
      ? JoiError.details[0].message
      : 'Invalid input';
    this.status = 400;
  }
};
