module.exports = class WrongIdFormatError extends Error {
  constructor(MongoCollectionSaveError) {
    super(MongoCollectionSaveError);
    this.name = 'MongoCollectionSaveError';
    this.message = MongoCollectionSaveError.message
      ? MongoCollectionSaveError.message
      : 'Something went wrong while saving';
    this.status = 500;
  }
};
