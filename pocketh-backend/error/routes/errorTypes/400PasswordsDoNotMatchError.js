module.exports = class PasswordsDoNotMatchError extends Error {
  constructor() {
    super();
    this.name = 'PasswordsDoNotMatchError';
    this.message = 'Passwords do not match';
    this.status = 400;
  }
};
