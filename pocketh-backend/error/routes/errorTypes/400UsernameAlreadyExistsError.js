module.exports = class UsernameAlreadyExistsError extends Error {
  constructor() {
    super();
    this.name = 'UsernameAlreadyExistsError';
    this.message = 'Username already exists';
    this.status = 400;
  }
};
