module.exports = class IdNotFoundError extends Error {
  constructor() {
    super();
    this.name = 'IdNotFoundError';
    this.message = 'Id not found';
    this.status = 404;
  }
};
