const JwtExpiredError = require('../../../error/middleware/errorTypes/400JwtExpiredError');

module.exports = jwtDecoded => {
  const issuedAt = jwtDecoded.iat;
  const secondsUntilExpiration = 1800;
  const currentDateInSecods = Date.now() / 1000;
  const isExpired = issuedAt + secondsUntilExpiration < currentDateInSecods;
  if (isExpired) throw new JwtExpiredError();
  return;
};
