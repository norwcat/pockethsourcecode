const NoJwtError = require('../../../error/middleware/errorTypes/400NoJwtError');

module.exports = req => {
  const jwt = req.header('x-auth-token');
  if (!jwt) throw new NoJwtError();
  return jwt;
};
