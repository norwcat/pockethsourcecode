const jsonwebtoken = require('jsonwebtoken');
const config = require('config');
const JwtExpiredError = require('../../../error/middleware/errorTypes/400JwtExpiredError');
const InvalidJwtError = require('../../../error/middleware/errorTypes/400InvalidJwtError');

module.exports = async jwt => {
  let verifiedAndDecodedJwt;
  try {
    verifiedAndDecodedJwt = await jsonwebtoken.verify(
      jwt,
      config.get('jwtPrivateKey')
    );
  } catch (e) {
    if (e.name === 'JsonWebTokenError') throw new InvalidJwtError();
    if (e.name === 'TokenExpiredError') throw new JwtExpiredError();
  }
  return verifiedAndDecodedJwt;
};
