const checkReqForJwtReturnJwt = require('../helper/checkReqForJwtReturnJwt');
const verifyJwtReturnDecodedJwt = require('../helper/verifyJwtReturnDecodedJwt');
const appendJwtDecodedToReq = require('../helper/appendJwtDecodedToReq');

module.exports = async (req, res, next) => {
  try {
    const jwt = checkReqForJwtReturnJwt(req);
    const jwtDecoded = await verifyJwtReturnDecodedJwt(jwt);
    appendJwtDecodedToReq(jwtDecoded, req);
    next();
  } catch (e) {
    return res.status(e.status).send(e.message);
  }
};
