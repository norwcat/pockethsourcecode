const NotAUserError = require('../../../error/middleware/errorTypes/400NotAUserError');
const InvalidJwtError = require('../../../error/middleware/errorTypes/400InvalidJwtError');

module.exports = (req, res, next) => {
  try {
    if (!req || !req.jwtDecoded || !req.jwtDecoded.typeOfRequestor)
      throw new InvalidJwtError();
    if (req.jwtDecoded.typeOfRequestor !== 'user') throw new NotAUserError();
    next();
  } catch (e) {
    return res.status(e.status).send(e.message);
  }
};
