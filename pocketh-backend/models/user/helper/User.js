const mongoose = require('mongoose');
const userSchemaMongoose = require('./userSchemaMongoose');

module.exports = mongoose.model('User', userSchemaMongoose);
