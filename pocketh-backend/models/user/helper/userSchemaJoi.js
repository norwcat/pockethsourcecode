const Joi = require('joi');

module.exports = {
  username: Joi.string()
    .min(4)
    .max(255)
    .required(),
  email: Joi.string()
    .min(5)
    .max(255)
    .required()
    .email(),
  password: Joi.string()
    .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{4,})/)
    .required()
};
