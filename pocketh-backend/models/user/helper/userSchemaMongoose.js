const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
  creationTimestamp: { type: Date, default: Date.now },
  username: { type: String, minlength: 4, maxlength: 255, required: true },
  email: { type: String, minlength: 5, maxlength: 255, required: true },
  password: {
    type: String,
    minlength: 60,
    maxlength: 60,
    required: true,
  },
  type: { type: String, default: 'user' },
  positions: { type: Object, default: {} },
});
