const findUserByEmail = require('./findUserByEmail');
const IdNotFoundError = require('../../../error/routes/errorTypes/404IdNotFoundError');
const EmailAlreadyExistsError = require('../../../error/routes/errorTypes/400EmailAlreadyExistsError');

module.exports = async email => {
  try {
    await findUserByEmail(email);
  } catch (e) {
    if (e instanceof IdNotFoundError) {
      return;
    } else {
      throw e;
    }
  }
  throw new EmailAlreadyExistsError();
};
