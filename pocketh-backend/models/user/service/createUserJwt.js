const config = require('config');
const jwt = require('jsonwebtoken');

module.exports = (user) => {
  const jwtPrivateKey = config.get('jwtPrivateKey');
  const signedJwt = jwt.sign(
    {
      typeOfRequestor: user.type,
      _id: user._id,
      username: user.username,
      exp: Math.floor(Date.now() / 1000) + 60 * 15,
    },
    jwtPrivateKey,
    {
      algorithm: 'HS256',
    }
  );
  return signedJwt;
};
