const User = require('../helper/User');
const InvalidIdError = require('../../../error/routes/errorTypes/400InvalidIdError');
const IdNotFoundError = require('../../../error/routes/errorTypes/404IdNotFoundError');

module.exports = async id => {
  let userFound;
  try {
    userFound = await User.findById(id);
  } catch (e) {
    throw new InvalidIdError();
  }
  if (!userFound) throw new IdNotFoundError();
  return userFound;
};
