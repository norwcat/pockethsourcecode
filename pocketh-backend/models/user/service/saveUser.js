const User = require('../helper/User');
const MongoCollectionSaveError = require('../../../error/routes/errorTypes/400MongoCollectionSaveError');
const bcrypt = require('bcryptjs');

module.exports = async user => {
  let userSaved;
  try {
    tempUser = { ...user, password: await bcrypt.hash(user.password, 12) };
    userSaved = await new User(tempUser).save();
  } catch (e) {
    throw new MongoCollectionSaveError(e);
  }
  return userSaved;
};
