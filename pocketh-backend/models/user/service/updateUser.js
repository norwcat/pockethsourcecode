const User = require('../helper/User');
const IdNotFoundError = require('../../../error/routes/errorTypes/404IdNotFoundError');
const MongoCollectionSaveError = require('../../../error/routes/errorTypes/400MongoCollectionSaveError');

module.exports = async (id, userData) => {
  let updatedUser;
  try {
    updatedUser = await User.findByIdAndUpdate(
      id,
      { $set: userData },
      {
        new: true,
      }
    );
  } catch (e) {
    throw new MongoCollectionSaveError();
  }
  if (!updatedUser) throw new IdNotFoundError();
  return updatedUser;
};
