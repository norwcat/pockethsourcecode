const Joi = require('joi');
const userSchemaJoi = require('../helper/userSchemaJoi');
const JoiError = require('../../../error/routes/errorTypes/400JoiError');

module.exports = user => {
  const { error } = Joi.validate(user, userSchemaJoi);
  if (error) throw new JoiError(error);
  return;
};
