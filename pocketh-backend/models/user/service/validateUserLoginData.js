const Joi = require('joi');
const userSchemaJoi = require('../helper/userSchemaJoi');
const JoiError = require('../../../error/routes/errorTypes/400JoiError');

module.exports = loginData => {
  const loginDataSchema = {
    username: userSchemaJoi.username,
    password: userSchemaJoi.password
  };
  const { error } = Joi.validate(loginData, loginDataSchema);
  if (error) throw new JoiError(error);
  return;
};
