const Joi = require('joi');
const userSchemaJoi = require('../helper/userSchemaJoi');
const JoiError = require('../../../error/routes/errorTypes/400JoiError');

module.exports = username => {
  const usernameSchema = { username: userSchemaJoi.username };
  username = { username }; //Joi only validates Objects
  const { error } = Joi.validate(username, usernameSchema);
  if (error) throw new JoiError(error);
  return;
};
