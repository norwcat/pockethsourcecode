const checkIfUserLoginDataIsValid = require('./checkIfUserLoginDataIsValid');
const findUserByUsername = require('../../../models/user/service/findUserByUsername');
const checkIfPasswordOfUserMatchesLoginData = require('./checkIfPasswordOfUserMatchesLoginData');
const createUserJwt = require('../../../models/user/service/createUserJwt');

module.exports = async (loginData) => {
  try {
    checkIfUserLoginDataIsValid(loginData);
    const username = loginData.username;
    const user = await findUserByUsername(username);
    await checkIfPasswordOfUserMatchesLoginData(user, loginData);
    return createUserJwt(user);
  } catch (e) {
    throw e;
  }
};
