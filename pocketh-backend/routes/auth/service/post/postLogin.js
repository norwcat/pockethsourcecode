const express = require('express');
const router = express.Router();
const authenticateUserAndCheckUserLoginDataGetUserJwt = require('../../helper/authenticateUserAndCheckUserLoginDataGetUserJwt');

router.post('/', async (req, res) => {
  try {
    const loginData = req.body;
    let jwt;
    jwt = await authenticateUserAndCheckUserLoginDataGetUserJwt(loginData);
    if (jwt) res.send({ jwt });
  } catch (e) {
    return res.status(e.status).send(e.message);
  }
});

module.exports = router;
