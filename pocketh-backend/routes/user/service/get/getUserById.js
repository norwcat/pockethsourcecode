const express = require('express');
const router = express.Router();
const auth = require('../../../../middleware/auth/service/auth');
const user = require('../../../../middleware/user/service/user');
const findUserById = require('../../../../models/user/service/findUserById');

module.exports = router.get('/', [auth, user], async (req, res) => {
  try {
    const id = req.id;
    const userFound = await findUserById(id);

    if (userFound) res.send(userFound);
  } catch (e) {
    return res.status(e.status).send(e.message);
  }
});
