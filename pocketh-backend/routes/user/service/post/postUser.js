const express = require('express');
const router = express.Router();
const validateUser = require('../../../../models/user/service/validateUser');
const checkIfUserExistsByUsername = require('../../../../models/user/service/checkIfUserExistsByUsername');
const checkIfUserExistsByEmail = require('../../../../models/user/service/checkIfUserExistsByEmail');
const saveUser = require('../../../../models/user/service/saveUser');
const createUserJwt = require('../../../../models/user/service/createUserJwt');

module.exports = router.post('/', async (req, res) => {
  try {
    const user = req.body;
    validateUser(user);

    const username = user.username;
    await checkIfUserExistsByUsername(username);

    const email = user.email;
    await checkIfUserExistsByEmail(email);

    const savedUser = await saveUser(user);

    const jwt = await createUserJwt(savedUser);
    const loggedInUser = { ...savedUser._doc, jwt };

    return res.send(loggedInUser);
  } catch (e) {
    return res.status(e.status).send(e.message);
  }
});
