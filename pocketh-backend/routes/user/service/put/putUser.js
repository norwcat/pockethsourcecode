const express = require('express');
const router = express.Router();
const User = require('../../../../models/user/helper/User');
const validateUser = require('../../../../models/user/service/validateUser');
const updateUser = require('../../../../models/user/service/updateUser');

module.exports = router.put('/', [auth, user], async (req, res) => {
  try {
    const userData = req.body;
    validateUser(userData);

    const id = req.id;
    const updatedUser = await updateUser(id, userData);

    return res.send(updatedUser);
  } catch (e) {
    return res.status(e.status).send(e.message);
  }
});
