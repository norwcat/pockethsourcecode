const winston = require('winston');
const express = require('express');
const app = express();
console.log(process.env.NODE_ENV);
require('./startup/service/logging')();
require('./startup/service/routes')(app);
require('./startup/service/db')();
require('./startup/service/config')();
require('./startup/service/validation')();

const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
  console.log(`Listening on Port ${port}...`);
  winston.info(`Listening on Port ${port}...`);
});

module.exports = server;
