const config = require('config');
const winston = require('winston');
const express = require('express');
const app = express();

require('./startup/service/logging')();
require('./startup/service/routes')(app);
require('./startup/service/db')();
require('./startup/service/config')();
require('./startup/service/validation')();

const port = 55554;
const server = app.listen(port, () => {
  console.log(`Listening on Port ${port}...`);
  winston.info(`Listening on Port ${port}...`);
});

module.exports = server;
module.exports.app = app;
