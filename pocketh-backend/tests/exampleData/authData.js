const testUser = require('./userData').testUser;

module.exports.correctLoginData = {
  username: testUser.username,
  password: testUser.password
};

const incorrectLoginDataWrongPassword = { ...this.correctLoginData };
incorrectLoginDataWrongPassword.password = 't0sT!';
module.exports.incorrectLoginDataWrongPassword = incorrectLoginDataWrongPassword;

const unknownUsername = 'unknwonUsername';
module.exports.unknownUsername = unknownUsername;

const incorrectLoginDataUnknownUsername = { ...this.correctLoginData };
incorrectLoginDataUnknownUsername.username = this.unknownUsername;
module.exports.incorrectLoginDataUnknownUsername = incorrectLoginDataUnknownUsername;
