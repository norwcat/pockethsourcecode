const User = require('../../../../models/user/helper/User');
const saveUser = require('../../../../models/user/service/saveUser');
const createUserJwt = require('../../../../models/user/service/createUserJwt');
const authMiddleware = require('../../../../middleware/auth/service/auth');
const jwt = require('jsonwebtoken');
const config = require('config');

describe('auth middleware', () => {
  let server;
  let testUser;
  let jwtCreated;
  let req;
  let res;
  let next;

  backDateJWTBy16Mins = () => {
    let jwtDecoded = jwt.verify(jwtCreated, config.get('jwtPrivateKey'));
    jwtDecoded.exp = jwtDecoded.exp - 60 * 16;
    jwtCreated = jwt.sign(jwtDecoded, config.get('jwtPrivateKey'));
  };

  beforeAll(async () => {
    testUser = require('../../../exampleData/userData').testUser;
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
    secondsUntilExpiration = 1800;
    testUserInit = await saveUser(testUser);
    xAuthToken = 'x-auth-token';
    jwtCreated = await createUserJwt(testUserInit);
    req = {
      header: () => {
        return jwtCreated;
      }
    };
    res = {
      send: body => {
        res.body = body;
      },
      status: status => {
        res.status = status;
        // This next line makes it chainable (normally done w/ return this)
        return res;
      }
    };
    next = jest.fn();
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  describe('PASS', () => {
    it('should CREATE AND CHECK JWT', async () => {
      await authMiddleware(req, res, next);
      expect(res.status instanceof Function).toBe(true);
      expect(res.body).toBe(undefined);
      expect(res.jwtDecoded).not.toBe(null);
      expect(next).toHaveBeenCalledTimes(1);
    });
  });
  describe('FAIL', () => {
    it('should return 400 BAD REQUEST NO JWT ', async () => {
      req.header = jest.fn();
      await authMiddleware(req, res, next);
      expect(res.status).toBe(400);
      expect(res.body).toContain('No');
      expect(next).toHaveBeenCalledTimes(0);
    });
    it('should return 400 BAD REQUEST INVALID JWT CONTENT', async () => {
      jwtCreated = jwt.sign('test', 'test');
      await authMiddleware(req, res, next);
      expect(res.status).toBe(400);
      expect(res.body).toContain('invalid');
      expect(next).toHaveBeenCalledTimes(0);
    });
    it('should return 400 BAD REQUEST INVALID JWT CONTENT', async () => {
      backDateJWTBy16Mins();
      await authMiddleware(req, res, next);
      expect(res.status).toBe(400);
      expect(res.body).toContain('expired');
      expect(next).toHaveBeenCalledTimes(0);
    });
  });
});
