const bcrypt = require('bcryptjs');
const authMiddleware = require('../../../../middleware/user/service/user');

describe('user authorization middleware', () => {
  let plainTextPassword;
  let saltRounds;
  let server;
  let req;
  let res;
  let next;

  beforeAll(async () => {
    plainTextPassword = 'password';
    saltRounds = 12;
    passwordHashedInit = await bcrypt.hash(plainTextPassword, saltRounds);
    testUsername = 'norwcat';
    testEmail = 'norwcat@gmail.com';
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
    req = {};
    res = {
      send: body => {
        res.body = body;
      },
      status: status => {
        res.code = status;
        // This next line makes it chainable (usually done w/ return this)
        return res;
      }
    };
    next = jest.fn();
  });

  afterEach(async () => {
    await server.close();
  });
  describe('PASS', () => {
    it('should CALL NEXT', async () => {
      req.jwtDecoded = { typeOfRequestor: 'user' };
      await authMiddleware(req, res, next);
      expect(next).toHaveBeenCalledTimes(1);
    });
  });
  describe('FAIL', () => {
    it('should NOT CALL NEXT RETURN BAD REQUEST 400 NOT LOGGED IN USER', async () => {
      req.jwtDecoded = { typeOfRequestor: 'notAUser' };
      await authMiddleware(req, res, next);
      expect(next).toHaveBeenCalledTimes(0);
      expect(res.code).toBe(400);
      expect(res.body).toContain('Not a logged in user');
    });
  });
});
