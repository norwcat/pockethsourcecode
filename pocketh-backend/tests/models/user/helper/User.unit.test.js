const bcrypt = require('bcryptjs');
const User = require('../../../../models/user/helper/User');

describe('User model', () => {
  let plainTextPassword;
  let saltRounds;
  let passwordHashedInit;
  let server;
  let testUser;
  let testUserEmpty;
  let exception;

  beforeAll(async () => {
    plainTextPassword = 'password';
    saltRounds = 12;
    passwordHashedInit = await bcrypt.hash(plainTextPassword, saltRounds);
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
    testUser = {
      username: 'norwcat',
      email: 'norwcat@gmail.com',
      password: passwordHashedInit,
    };
    testUserLoginData = {
      username: testUser.username,
      password: testUser.passwordHashed,
    };
    testUserEmpty = {};
    exception = undefined;
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  equalizeTestUserInit = (testUserInit) => {
    testUser._id = testUserInit._id;
    testUser.__v = testUserInit.__v;
    testUser.creationTimestamp = testUserInit.creationTimestamp;
    testUser.type = testUserInit.type;
    testUser.positions = testUserInit.positions;
  };

  describe(' PASS', () => {
    it('should CREATE NEW USER', async () => {
      const testUserInit = await new User(testUser).save();
      equalizeTestUserInit(testUserInit);
      expect(testUserInit._doc).toEqual(testUser);
    });
  });
  describe('User FAIL', () => {
    it('should NOT SAVE ALL EMPTY 400 BAD REQUEST', async () => {
      try {
        await new User(testUserEmpty).save();
      } catch (ex) {
        exception = ex;
      }
      expect(exception).not.toBe(undefined);
    });
    it('should NOT SAVE USERNAME EMPTY 400 BAD REQUEST', async () => {
      testUser.username = undefined;
      try {
        await new User(testUser).save();
      } catch (ex) {
        exception = ex;
      }
      expect(exception).not.toBe(undefined);
    });
    it('should NOT SAVE EMAIL EMPTY 400 BAD REQUEST', async () => {
      testUser.email = undefined;
      try {
        await new User(testUser).save();
      } catch (ex) {
        exception = ex;
      }
      expect(exception).not.toBe(undefined);
    });
    it('should NOT SAVE PASSWORDHASHED EMPTY 400 BAD REQUEST', async () => {
      testUser.password = undefined;
      try {
        await new User(testUser).save();
      } catch (ex) {
        exception = ex;
      }
      expect(exception).not.toBe(undefined);
    });
  });
});
