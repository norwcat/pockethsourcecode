const saveUser = require('../../../../models/user/service/saveUser');
const User = require('../../../../models/user/helper/User');
const checkIfUserExistsByUsername = require('../../../../models/user/service/checkIfUserExistsByUsername');

describe('User model', () => {
  let server;
  let testUser;
  let correctLoginData;
  let incorrectLoginDataWrongPassword;
  let exception;

  beforeAll(async () => {
    server = require('../../../../testServer');
    testUser = require('../../../exampleData/userData').testUser;
    correctLoginData = require('../../../exampleData/authData')
      .correctLoginData;
    incorrectLoginDataWrongPassword = require('../../../exampleData/authData')
      .incorrectLoginDataWrongPassword;
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
    exception = {};
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  equalizeTestUserInit = (testUserInit) => {
    testUser._id = testUserInit._id;
    testUser.__v = testUserInit.__v;
    testUser.creationTimestamp = testUserInit.creationTimestamp;
    testUser.type = testUserInit.type;
    testUser.positions = testUserInit.positions;
  };

  describe('PASS', () => {
    it('should NOT FIND USER WHILE CHECKING BY EMAIL', async () => {
      validUsername = testUser.email;
      const testUserChecked = await checkIfUserExistsByUsername(validUsername);
      expect(testUserChecked).toBe(undefined);
    });
  });
  describe('FAIL', () => {
    it('should FIND USER WHILE CHECKING BY EMAIL', async () => {
      const testUserInit = await saveUser(testUser);
      validUsername = testUserInit.username;
      try {
        await checkIfUserExistsByUsername(validUsername);
      } catch (e) {
        exception = e;
      }
      expect(exception.status).toBe(400);
      expect(exception.message).toContain('already exists');
    });
    it('should THROW INVALID EMAIL/ID', async () => {
      invalidUsername = '';
      try {
        await checkIfUserExistsByUsername(invalidUsername);
      } catch (e) {
        exception = e;
      }
      expect(exception.status).toBe(undefined);
      expect(exception.message).toBe(undefined);
    });
  });
});
