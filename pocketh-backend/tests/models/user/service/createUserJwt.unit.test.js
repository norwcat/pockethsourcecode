const bcrypt = require('bcryptjs');
const User = require('../../../../models/user/helper/User');
const findUserByEmail = require('../../../../models/user/service/findUserByEmail');

describe('User model', () => {
  let plainTextPassword;
  let saltRounds;
  let passwordHashedInit;
  let validId;
  let inValidId;
  let server;
  let testUser;
  let testUserLoginData;
  let testUserEmpty;
  let wrongPassword;
  let unknownUsername;

  beforeAll(async () => {
    plainTextPassword = 'password';
    saltRounds = 12;
    passwordHashedInit = await bcrypt.hash(plainTextPassword, saltRounds);
    validId = '5dd05ad0e4b3a6032df0f4f0';
    inValidId = '0123456789';
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
    testUser = {
      username: 'norwcat',
      email: 'norwcat@gmail.com',
      passwordHashed: passwordHashedInit,
    };
    testUserLoginData = {
      username: testUser.username,
      passwordHashed: testUser.passwordHashed,
    };
    testUserEmpty = {};
    wrongPassword =
      '$2y$12$YPKv9TAFWm88LulB3ti4RuPpiMLIau5Hnk7a5D9QQdZ96yht23Dtt';
    unknownUsername = 'unknown';
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  equalizeTestUserInit = (testUserInit) => {
    testUser._id = testUserInit._id;
    testUser.__v = testUserInit.__v;
    testUser.creationTimestamp = testUserInit.creationTimestamp;
    testUser.type = testUserInit.type;
    testUser.positions = testUserInit.positions;
  };

  describe('PASS', () => {
    it('', async () => {});
  });
  describe('FAIL', () => {
    it('', async () => {});
  });
});
