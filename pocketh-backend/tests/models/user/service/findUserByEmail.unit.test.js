const saveUser = require('../../../../models/user/service/saveUser');
const User = require('../../../../models/user/helper/User');
const findUserByEmail = require('../../../../models/user/service/findUserByEmail');

describe('User model', () => {
  let server;
  let testUser;
  let correctLoginData;
  let incorrectLoginDataWrongPassword;
  let exception;

  beforeAll(async () => {
    server = require('../../../../testServer');
    testUser = require('../../../exampleData/userData').testUser;
    correctLoginData = require('../../../exampleData/authData')
      .correctLoginData;
    incorrectLoginDataWrongPassword = require('../../../exampleData/authData')
      .incorrectLoginDataWrongPassword;
    exception = {};
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
    exception = {};
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  equalizeTestUserInit = (testUserInit) => {
    testUser._id = testUserInit._id;
    testUser.__v = testUserInit.__v;
    testUser.creationTimestamp = testUserInit.creationTimestamp;
    testUser.type = testUserInit.type;
    testUser.positions = testUserInit.positions;
  };

  describe('PASS', () => {
    it('should FIND USER WHILE CHECKING BY EMAIL', async () => {
      const testUserInit = await saveUser(testUser);
      validEmail = testUser.email;
      const testUserFound = await findUserByEmail(validEmail);
      expect(testUserInit._doc).toEqual(testUserFound._doc);
    });
  });
  describe('FAIL', () => {
    it('should NOT FIND USER WHILE CHECKING BY EMAIL', async () => {
      validEmail = testUser.email;
      try {
        await findUserByEmail(validEmail);
      } catch (e) {
        exception.status = e.status;
        exception.message = e.message;
      }
      expect(exception.status).toBe(404);
      expect(exception.message).toContain('not found');
    });
    it('should THROW INVALID EMAIL/ID', async () => {
      invalidEmail = '';
      let noUser = await User.findOne({ invalidEmail });
      expect(noUser).toBe(null);
    });
  });
});
