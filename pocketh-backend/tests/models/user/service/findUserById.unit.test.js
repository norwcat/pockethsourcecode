const saveUser = require('../../../../models/user/service/saveUser');
const User = require('../../../../models/user/helper/User');
const findUserById = require('../../../../models/user/service/findUserById');

describe('User model', () => {
  let server;
  let testUser;
  let correctLoginData;
  let incorrectLoginDataWrongPassword;
  let exception;
  let validId;
  let inValidId;

  beforeAll(async () => {
    server = require('../../../../testServer');
    testUser = require('../../../exampleData/userData').testUser;
    correctLoginData = require('../../../exampleData/authData')
      .correctLoginData;
    incorrectLoginDataWrongPassword = require('../../../exampleData/authData')
      .incorrectLoginDataWrongPassword;
    exception = {};
    validId = '5dd05ad0e4b3a6032df0f4f0';
    inValidId = '0123456789';
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
    exception = {};
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  equalizeTestUserInit = (testUserInit) => {
    testUser._id = testUserInit._id;
    testUser.__v = testUserInit.__v;
    testUser.creationTimestamp = testUserInit.creationTimestamp;
    testUser.type = testUserInit.type;
    testUser.positions = testUserInit.positions;
  };

  describe('PASS', () => {
    it('should FIND CREATED USER BY ID', async () => {
      const testUserInit = await saveUser(testUser);
      const id = testUserInit._id;
      const testUserFound = await findUserById(id);
      expect(testUserFound._id).toEqual(testUserInit._id);
    });
  });
  describe('FAIL', () => {
    it('should NOT FIND USER 404 NOT FOUND', async () => {
      try {
        await findUserById(validId);
      } catch (e) {
        exception = e;
      }
      expect(exception.status).toBe(404);
      expect(exception.message).toContain('not found');
    });
    it('should NOT FIND USER 400 INVALID ID', async () => {
      try {
        await findUserById(inValidId);
      } catch (e) {
        exception = e;
      }
      expect(exception.status).toBe(400);
      expect(exception.message).toContain('Invalid id');
    });
  });
});
