const saveUser = require('../../../../models/user/service/saveUser');
const User = require('../../../../models/user/helper/User');
const findUserByUsername = require('../../../../models/user/service/findUserByUsername');

describe('User model', () => {
  let server;
  let testUser;
  let correctLoginData;
  let incorrectLoginDataWrongPassword;

  beforeAll(async () => {
    server = require('../../../../testServer');
    testUser = require('../../../exampleData/userData').testUser;
    correctLoginData = require('../../../exampleData/authData')
      .correctLoginData;
    incorrectLoginDataWrongPassword = require('../../../exampleData/authData')
      .incorrectLoginDataWrongPassword;
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  equalizeTestUserInit = (testUserInit) => {
    testUser._id = testUserInit._id;
    testUser.__v = testUserInit.__v;
    testUser.creationTimestamp = testUserInit.creationTimestamp;
    testUser.type = testUserInit.type;
    testUser.positions = testUserInit.positions;
  };

  describe('PASS', () => {
    it('should FIND USER WHILE CHECKING BY USERNAME', async () => {
      const testUserInit = await saveUser(testUser);
      validUsername = testUserInit.username;
      const testUserFound = await findUserByUsername(validUsername);
      expect(testUserInit._doc).toEqual(testUserFound._doc);
    });
  });
  describe('FAIL', () => {
    it('should NOT FIND USER WHILE CHECKING BY USERNAME', async () => {
      validUsername = testUser.username;
      try {
        await findUserByUsername(validUsername);
      } catch (e) {
        expect(e.status).toBe(404);
        expect(e.message).toContain('not found');
      }
    });
    /*     it('should THROW INVALID USERNAME/ID', async () => {
      inValidUsername = '';
      try {
        await findUserByUsername(inValidUsername);
      } catch (e) {
        expect(e.status).toBe(400);
        expect(e.message).toContain('Invalid id');
      }
    });
    Won't throw because username is not checked against schema before exectuing search
 */
  });
});
