const bcrypt = require('bcryptjs');
const User = require('../../../../models/user/helper/User');
const saveUser = require('../../../../models/user/service/saveUser');

describe('User model', () => {
  let server;
  let testUser;
  let correctLoginData;
  let testUserEmpty;

  beforeAll(async () => {
    testUser = require('../../../exampleData/userData').testUser;
    correctLoginData = require('../../../exampleData/authData')
      .correctLoginData;
    testUserEmpty = {};
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  equalizeTestUserInit = (testUserInit) => {
    testUser._id = testUserInit._id;
    testUser.__v = testUserInit.__v;
    testUser.creationTimestamp = testUserInit.creationTimestamp;
    testUser.type = testUserInit.type;
    testUser.positions = testUserInit.positions;
  };

  describe(' PASS', () => {
    it('should CREATE NEW USER', async () => {
      const testUserInit = await saveUser(testUser);
      equalizeTestUserInit(testUserInit);
      expect({ ...testUserInit._doc, password: testUser.password }).toEqual(
        testUser
      );
      expect(
        await bcrypt.compare(testUser.password, testUserInit._doc.password)
      ).toBeTruthy();
      return;
    });
    describe('User FAIL', () => {
      it('should NOT SAVE ALL EMPTY 400 BAD REQUEST', async () => {
        let schemaError;
        try {
          await new User(testUserEmpty);
        } catch (ex) {
          schemaError = ex;
        }
        expect(schemaError).not.toBe(null);
      });
      it('should NOT SAVE USERNAME EMPTY 400 BAD REQUEST', async () => {
        testUser.username = undefined;
        let schemaError;
        try {
          await saveUser(testUser);
        } catch (ex) {
          schemaError = ex;
        }
        expect(schemaError).not.toBe(null);
      });
      it('should NOT SAVE EMAIL EMPTY 400 BAD REQUEST', async () => {
        testUser.email = undefined;
        let schemaError;
        try {
          await saveUser(testUser);
        } catch (ex) {
          schemaError = ex;
        }
        expect(schemaError).not.toBe(null);
      });
      it('should NOT SAVE PASSWORDHASHED EMPTY 400 BAD REQUEST', async () => {
        testUser.passwordHashed = undefined;
        let schemaError;
        try {
          await saveUser(testUser);
        } catch (ex) {
          schemaError = ex;
        }
        expect(schemaError).not.toBe(null);
      });
    });
  });
});
