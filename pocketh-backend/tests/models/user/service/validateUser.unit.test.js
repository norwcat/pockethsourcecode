const bcrypt = require('bcryptjs');
const validateUser = require('../../../../models/user/service/validateUser');

describe('User model', () => {
  let server;
  let testUser;
  let correctLoginData;
  let testUserEmpty;

  beforeAll(async () => {
    server = require('../../../../testServer');
    testUser = require('../../../exampleData/userData').testUser;
    correctLoginData = require('../../../exampleData/authData')
      .correctLoginData;
    testUserEmpty = {};
  });

  afterEach(async () => {
    await server.close();
  });

  describe(' PASS', () => {
    it('should VALIDATE USER', () => {
      expect(() => validateUser(testUser)).not.toThrow();
    });
    describe('FAIL', () => {
      it('should NOT VALIDATE ALL EMPTY', () => {
        let validationError;
        try {
          validateUser(testUserEmpty);
        } catch (ex) {
          validationError = ex;
        }
        expect(validationError.message).not.toBe(null);
        expect(validationError.status).toBe(400);
      });
      it('should NOT VALIDATE USERNAME EMPTY', () => {
        testUser.username = undefined;
        let validationError;
        try {
          validateUser(testUser);
        } catch (ex) {
          validationError = ex;
        }
        expect(validationError.message).not.toBe(null);
        expect(validationError.status).toBe(400);
      });
      it('should NOT VALIDATE EMAIL EMPTY', () => {
        testUser.email = undefined;
        let validationError;
        try {
          validateUser(testUser);
        } catch (ex) {
          validationError = ex;
        }
        expect(validationError.message).not.toBe(null);
        expect(validationError.status).toBe(400);
      });
      it('should NOT VALIDATE PASSWORDHASHED EMPTY', () => {
        testUser.passwordHashed = undefined;
        let validationError;
        try {
          validateUser(testUser);
        } catch (ex) {
          validationError = ex;
        }
        expect(validationError.message).not.toBe(null);
        expect(validationError.status).toBe(400);
      });
      it('should NOT VALIDATE USERNAME EMPTY', () => {
        const invalidUsername = null;
        let validationError;
        try {
          validateUser(invalidUsername);
        } catch (ex) {
          validationError = ex;
        }
        expect(validationError.message).not.toBe(null);
        expect(validationError.status).toBe(400);
      });
      it('should NOT VALIDATE USERNAME < 4 400 BAD REQUEST', () => {
        const invalidUsername = 'abc';
        let validationError;
        try {
          validateUser(invalidUsername);
        } catch (ex) {
          validationError = ex;
        }
        expect(validationError.message).not.toBe(null);
        expect(validationError.status).toBe(400);
      });
      it('should NOT VALIDATE USERNAME > 255 400 BAD REQUEST', () => {
        const invalidUsername =
          'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz';
        let validationError;
        try {
          validateUser(invalidUsername);
        } catch (ex) {
          validationError = ex;
        }
        expect(validationError.message).not.toBe(null);
        expect(validationError.status).toBe(400);
      });
    });
  });
});
