const validateUserLoginData = require('../../../../models/user/service/validateUserLoginData');

describe('User model', () => {
  let server;
  let testUser;
  let correctLoginData;
  let incorrectLoginDataWrongPassword;

  beforeAll(async () => {
    server = require('../../../../testServer');
    testUser = require('../../../exampleData/userData').testUser;
    correctLoginData = require('../../../exampleData/authData')
      .correctLoginData;
    incorrectLoginDataWrongPassword = require('../../../exampleData/authData')
      .incorrectLoginDataWrongPassword;
  });

  afterEach(async () => {
    await server.close();
  });
  afterAll(async () => {
    await server.close();
  });

  describe(' PASS', () => {
    it('should AUTHENTICATE AND CHECK LOGINDATA, RETURN JWT', async () => {
      expect(() => validateUserLoginData(correctLoginData)).not.toThrow();
    });
  });

  describe('User FAIL', () => {
    it('should NOT AUTHENTICATE AND CHECK LOGINDATA, RETURN JWT LOGIN 400 DATA INVALID', async () => {
      let validationError;
      try {
        await validateUserLoginData({
          ...correctLoginData,
          username: undefined
        });
      } catch (ex) {
        validationError = ex;
      }
      expect(validationError.status).toBe(400);
      expect(validationError.message).toContain('"username" is required');
    });
    it('should NOT AUTHENTICATE 400 USERNAME TOO SHORT', async () => {
      let validationError;
      try {
        await validateUserLoginData({ ...correctLoginData, username: '' });
      } catch (ex) {
        validationError = ex;
      }
      expect(validationError.status).toBe(400);
      expect(validationError.message).toContain(
        '"username" is not allowed to be empty'
      );
    });
    it('should NOT AUTHENTICATE 400 USERNAME TOO LONG', async () => {
      let validationError;
      try {
        await validateUserLoginData({
          ...correctLoginData,
          username:
            '123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324'
        });
      } catch (ex) {
        validationError = ex;
      }
      expect(validationError.status).toBe(400);
      expect(validationError.message).toContain(
        '"username" length must be less than or equal to 255 characters long'
      );
    });
    it('should NOT AUTHENTICATE AND CHECK LOGINDATA, NOT RETURN JWT LOGIN 400 INVALID PASSWORD', async () => {
      let wrongPasswordError;
      try {
        await validateUserLoginData({
          ...correctLoginData,
          password: 'test0!'
        });
      } catch (ex) {
        wrongPasswordError = ex;
      }
      expect(wrongPasswordError.status).toBe(400);
      expect(wrongPasswordError.message).toContain(
        'fails to match the required pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{4,})/'
      );
    });
  });
});
