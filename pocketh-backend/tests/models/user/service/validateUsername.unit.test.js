const bcrypt = require('bcryptjs');
const validateUsername = require('../../../../models/user/service/validateUsername');

describe('User model', () => {
  let plainTextPassword;
  let saltRounds;
  let passwordHashedInit;
  let server;
  let testUser;
  let wrongPassword;

  beforeAll(async () => {
    plainTextPassword = 'password';
    saltRounds = 12;
    passwordHashedInit = await bcrypt.hash(plainTextPassword, saltRounds);
  });

  beforeEach(async () => {
    server = require('../../../../testServer');
  });

  afterEach(async () => {
    await server.close();
  });

  describe(' PASS', () => {
    it('should VALIDATE USERNAME', async () => {
      expect(() => validateUsername('Paul')).not.toThrow();
    });
  });

  describe('User FAIL', () => {
    it('should THROW USERNAME TOO SHORT', async () => {
      let validationError;
      try {
        await validateUsername('');
      } catch (ex) {
        validationError = ex;
      }
      expect(validationError.status).toBe(400);
      expect(validationError.message).not.toBe(null);
    });
    it('should NOT AUTHENTICATE 400 USERNAME TOO LONG', async () => {
      let validationError;
      try {
        await validateUsername(
          '123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324123456789101112131415161718192021222324'
        );
      } catch (ex) {
        validationError = ex;
      }
      expect(validationError.status).toBe(400);
      expect(validationError.message).not.toBe(null);
    });
  });
});
