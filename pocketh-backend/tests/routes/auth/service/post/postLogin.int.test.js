const request = require('supertest');
const saveUser = require('../../../../../models/user/service/saveUser');
const User = require('../../../../../models/user/helper/User');
const jwt = require('jsonwebtoken');
const config = require('config');

describe('/api/login', () => {
  let server;
  let testUser;
  let correctLoginData;
  let incorrectLoginDataWrongPassword;
  let unknownUsername;
  let resEmpty;

  beforeAll(async () => {
    testUser = require('../../../../exampleData/userData').testUser;
    correctLoginData = require('../../../../exampleData/authData')
      .correctLoginData;
    incorrectLoginDataWrongPassword = require('../../../../exampleData/authData')
      .incorrectLoginDataWrongPassword;
    unknownUsername = require('../../../../exampleData/authData')
      .unknownUsername;
    resEmpty = {};
  });

  beforeEach(async () => {
    server = require('../../../../../testServer');
    testUser = require('../../../../exampleData/userData').testUser;
    await saveUser(testUser);
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  describe('POST', () => {
    describe('PASS', () => {
      it('gets salt, calculates hash, logs in/compares hashes, gets jwt, verifies jwt', async () => {
        const resLogin = await request(server)
          .post('/api/auth')
          .send(correctLoginData);
        const fetchedJwt = resLogin.body.jwt;
        const verifyJwt = jwt.verify(fetchedJwt, config.get('jwtPrivateKey'));
        expect(verifyJwt).toBeTruthy();
        expect(resLogin.status).toBe(200);
      });
    });
    describe('FAIL', () => {
      it('should return 404 USERNAME NOT FOUND', async () => {
        const userUnknownUsername = {
          ...correctLoginData,
          username: unknownUsername,
        };
        const res = await request(server)
          .post('/api/auth')
          .send(userUnknownUsername);
        expect(res.status).toBe(404);
        expect(res.text).toContain('not found');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 INCORRECT PASSWORD', async () => {
        const res = await request(server)
          .post('/api/auth')
          .send(incorrectLoginDataWrongPassword);
        expect(res.status).toBe(400);
        expect(res.text).toContain('do not match');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 USERNAME EMPTY/INVALID', async () => {
        const userNoUsername = {
          ...correctLoginData,
          username: null,
        };
        const res = await request(server)
          .post('/api/auth')
          .send(userNoUsername);
        expect(res.status).toBe(400);
        expect(res.text).toContain('must be');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 PASSWORD EMPTY/INVALID', async () => {
        const userNoPassword = {
          ...correctLoginData,
          password: undefined,
        };
        const res = await request(server)
          .post('/api/auth')
          .send(userNoPassword);
        expect(res.status).toBe(400);
        expect(res.text).toContain('is required');
        expect(res.body).toEqual(resEmpty);
      });
    });
  });
});
