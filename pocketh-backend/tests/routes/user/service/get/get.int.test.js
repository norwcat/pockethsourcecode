const request = require('supertest');

describe('/api/user', () => {
  let server;
  let resEmpty;

  beforeAll(async () => {
    server = require('../../../../../testServer');
    resEmpty = {};
  });

  afterAll(async () => {
    await server.close();
  });

  describe('GET / ', () => {
    describe('FAIL', () => {
      it('should return 400 BAD REQUEST EMPTY ID', async () => {
        const res = await request(server).get(`/api/user/`);
        expect(res.status).toBe(400);
        expect(res.text).toContain('Empty id');
        expect(res.body).toEqual(resEmpty);
      });
    });
  });
  describe('NOTHING /', () => {
    it('should do NOTHING', () => {
      expect(true).toBeTruthy();
    });
  });
});
