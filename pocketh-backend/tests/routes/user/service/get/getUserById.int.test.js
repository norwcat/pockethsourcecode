const request = require('supertest');
const User = require('../../../../../models/user/helper/User');
const saveUser = require('../../../../../models/user/service/saveUser');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('config');

describe('/api/user', () => {
  let server;
  let testUser;
  let correctLoginData;
  let incorrectLoginDataUnknownUsername;
  let resEmpty;

  beforeAll(async () => {
    testUser = require('../../../../exampleData/userData').testUser;
    correctLoginData = require('../../../../exampleData/authData')
      .correctLoginData;
    incorrectLoginDataUnknownUsername = require('../../../../exampleData/authData')
      .incorrectLoginDataUnknownUsername;
    resEmpty = {};
  });

  beforeEach(async () => {
    server = require('../../../../../testServer');
    await User.collection.deleteMany();
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });
  afterAll(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  describe('GET / ', () => {
    describe('PASS', () => {
      it('should return testUser', async () => {
        const testUserInit = await saveUser(testUser);
        const resAuthJwt = await request(server)
          .post('/api/auth')
          .send(correctLoginData);
        const authJwt = resAuthJwt.body.jwt;
        const res = await request(server)
          .get(`/api/user/${testUserInit._id}`)
          .set('x-auth-token', authJwt);
        expect(res.status).toBe(200);
        expect(res.body.email === testUser.email);
        expect(res.body.password === testUser.password);
      });
    });
    describe('FAIL', () => {
      it('should return 404 NOT FOUND DOES NOT EXIST', async () => {
        await saveUser(testUser);
        const resAuthJwt = await request(server)
          .post('/api/auth')
          .send(correctLoginData);
        const authJwt = resAuthJwt.body.jwt;
        const unknownUser = await new mongoose.Types.ObjectId();
        const res = await request(server)
          .get(`/api/user/${unknownUser}`)
          .set('x-auth-token', authJwt);
        expect(res.status).toBe(404);
        expect(res.text).toContain('not found');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 404 CAST ERROR', async () => {
        await saveUser(testUser);
        const resAuthJwt = await request(server)
          .post('/api/auth')
          .send(correctLoginData);
        const authJwt = resAuthJwt.body.jwt;
        const unknownUser = await new mongoose.Types.ObjectId();
        const res = await request(server)
          .get(`/api/user/${unknownUser}`)
          .set('x-auth-token', authJwt);
        expect(res.status).toBe(404);
        expect(res.text).toContain('not found');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 BAD REQUEST WRONG ID FORMAT', async () => {
        await saveUser(testUser);
        const resAuthJwt = await request(server)
          .post('/api/auth')
          .send(correctLoginData);
        const authJwt = resAuthJwt.body.jwt;
        const wrongIdFormat = '0123456789abcdef';
        const res = await request(server)
          .get(`/api/user/${wrongIdFormat}`)
          .set('x-auth-token', authJwt);
        expect(res.status).toBe(400);
        expect(res.text).toContain('Invalid id');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 NO JWT', async () => {
        const testUserInit = await saveUser(testUser);
        const res = await request(server).get(`/api/user/${testUserInit._id}`);
        expect(res.status).toBe(400);
        expect(res.body).not.toBe(null);
      });
      it('should return 400 WRONG JWT CONTENT', async () => {
        const testUserInit = await saveUser(testUser);
        const wrongContentAuthJwt = jwt.sign(
          { wrong: 'wrong' },
          config.get('jwtPrivateKey')
        );
        const res = await request(server)
          .get(`/api/user/${testUserInit._id}`)
          .set('x-auth-token', wrongContentAuthJwt);
        expect(res.status).toBe(400);
        expect(res.text).toContain('invalid');
        expect(res.body).not.toBe(null);
      });
      it('should return 400 WRONG JWT PRIVATE KEY / INVALID TOKEN', async () => {
        const testUserInit = await saveUser(testUser);
        let resAuthJwt = await request(server)
          .post('/api/auth')
          .send(correctLoginData);
        resAuthJwt = resAuthJwt.body.jwt;
        const decodedResAuthJwt = jwt.verify(
          resAuthJwt,
          config.get('jwtPrivateKey')
        );
        const wronglyEncodedJwt = jwt.sign(
          decodedResAuthJwt,
          'wrongJwtPrivateKey'
        );
        const res = await request(server)
          .get(`/api/user/${testUserInit._id}`)
          .set('x-auth-token', wronglyEncodedJwt);
        expect(res.status).toBe(400);
        expect(res.text).toContain('invalid');
        expect(res.body).not.toBe(null);
      });
    });
  });
});
