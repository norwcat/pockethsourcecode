const request = require('supertest');
const bcrypt = require('bcryptjs');
const User = require('../../../../../models/user/helper/User');
const saveUser = require('../../../../../models/user/service/saveUser');

describe('/api/user', () => {
  let server;
  let testUser;
  let testUserEmpty;
  let resEmpty;

  beforeAll(async () => {
    testUser = require('../../../../exampleData/userData').testUser;
    testUserEmpty = {};
    resEmpty = {};
  });

  beforeEach(() => {
    server = require('../../../../../testServer');
  });

  afterEach(async () => {
    await User.collection.deleteMany();
    await server.close();
  });
  afterAll(async () => {
    await User.collection.deleteMany();
    await server.close();
  });

  describe('POST / ', () => {
    describe('PASS', () => {
      it('should return created testUser', async () => {
        const res = await request(server)
          .post('/api/user')
          .send(testUser);
        expect(res.status).toBe(200);
        expect(res.body).not.toBe(null);
        expect(res.body._id).toBeTruthy();
        expect(res.body.email == testUser.email).toBeTruthy();
        expect(
          await bcrypt.compare(testUser.password, res.body.password)
        ).toBeTruthy();
      });
    });
    describe('FAIL', () => {
      it('should return 400 Bad request DUPLICATE EMAIL', async () => {
        await saveUser(testUser);
        const testUserDuplicateEmail = { ...testUser, username: 'John Doe' };
        const res = await request(server)
          .post('/api/user')
          .send(testUserDuplicateEmail);
        expect(res.status).toBe(400);
        expect(res.text).toContain('already exists');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 Bad request DUPLICATE USERNAME', async () => {
        const testUserInit = await saveUser(testUser);
        const testUserDuplicateUsername = {
          ...testUser,
          email: 'JohnDoe@gmail.com'
        };
        const res = await request(server)
          .post('/api/user')
          .send(testUserDuplicateUsername);
        expect(res.status).toBe(400);
        expect(res.text).toContain('already exists');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 Bad request ALL EMPTY', async () => {
        const res = await request(server)
          .post('/api/user')
          .send(testUserEmpty);
        expect(res.status).toBe(400);
        expect(res.text).toContain('is required');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 Bad request USERNAME EMPTY/INVALID', async () => {
        const testUserUsernameEmpty = { ...testUser };
        testUserUsernameEmpty.username = undefined;

        const res = await request(server)
          .post('/api/user')
          .send(testUserUsernameEmpty);
        expect(res.status).toBe(400);
        expect(res.text).toContain('is required');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 Bad request EMAIL EMPTY/INVALID', async () => {
        const testUserEmailEmpty = { ...testUser };
        testUserEmailEmpty.email = undefined;

        const res = await request(server)
          .post('/api/user')
          .send(testUserEmailEmpty);
        expect(res.status).toBe(400);
        expect(res.text).toContain('is required');
        expect(res.body).toEqual(resEmpty);
      });
      it('should return 400 Bad request PASSWORDHASHED EMPTY/INVALID', async () => {
        const testUserPasswordEmpty = { ...testUser };
        testUserPasswordEmpty.password = undefined;

        const res = await request(server)
          .post('/api/user')
          .send(testUserPasswordEmpty);
        expect(res.status).toBe(400);
        expect(res.text).toContain('is required');
        expect(res.body).toEqual(resEmpty);
      });
    });
  });
});
