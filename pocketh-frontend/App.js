const SignUpNavigator = createStackNavigator(
  {
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: { header: null }
    },
    EnableNotifications: {
      screen: EnableNotificationsScreen,
      navigationOptions: { header: null }
    },
    EnableLocation: {
      screen: EnableLocationScreen,
      navigationOptions: { header: null }
    }
  },
  {
    initialRouteName: 'SignUp'
  }
);

const LocationNavigator = createStackNavigator(
  {
    Location: {
      screen: LocationScreen,
      navigationOptions: { header: null }
    },
    LocationPictures: {
      screen: LocationPicturesScreen,
      navigationOptions: { header: null }
    },
    LocationInfo: {
      screen: LocationInfoScreen,
      navigationOptions: { header: null }
    },
    LocationReviews: {
      screen: LocationReviewsScreen,
      navigationOptions: { header: null }
    },
    LocationMap: {
      screen: LocationMapScreen,
      navigationOptions: { header: null }
    }
  },
  {
    initialRouteName: 'Location'
  }
);

const RegisterSendWyreNavigator = createStackNavigator(
  {
    RegisterPeronalData: {
      screen: RegisterPersonalDataScreen,
      navigationOptions: { header: null }
    },
    RegisterDriversLicense: {
      screen: RegisterDriversLicenseScreen,
      navigationOptions: { header: null }
    },
    RegisterExplanationDataSecurity: {
      screen: RegisterExplanationDataSecurityScreen,
      navigationOptions: { header: null }
    },
    RegisterBankAccount: {
      screen: RegisterBankAccountScreen,
      navigationOptions: { header: null }
    }
  },
  { initialRouteName: 'RegisterPersonalData' }
);

const PockethNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: { header: null }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: { header: null }
    },
    Help: {
      screen: HelpScreen,
      navigationOptions: { header: null }
    },
    About: {
      screen: AboutScreen,
      navigationOptions: { header: null }
    },
    QRScanner: {
      screen: QRScannerScreen,
      navigationOptions: { header: null }
    },
    TransactionHistory: {
      screen: TransactionHistoryScreen,
      navigationOptions: { header: null }
    },
    Location: {
      screen: LocationNavigator,
      navigationOptions: { header: null }
    },
    RegisterSendWyre: {
      screen: RegisterPersonalDataScreen,
      navigationOptions: { header: null }
    },
    RegisterDriversLicense: {
      screen: RegisterDriversLicenseScreen,
      navigationOptions: { header: null }
    },
    RegisterExplanationDataSecurity: {
      screen: RegisterExplanationDataSecurityScreen,
      navigationOptions: { header: null }
    },
    RegisterBankAccount: {
      screen: RegisterBankAccountScreen,
      navigationOptions: { header: null }
    }
  },
  { initialRouteName: 'Home', contentComponent: MainMenuDrawer, flex: 1 }
);

const AppNavigator = createAnimatedSwitchNavigator(
  {
    Intro: {
      screen: IntroScreen,
      navigationOptions: { header: null }
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: { header: null }
    },
    SignUp: {
      screen: SignUpNavigator,
      navigationOptions: { header: null }
    },
    Home: {
      screen: PockethNavigator,
      navigationOptions: { header: null }
    }
  },
  {
    initialRouteName: 'Intro'
  }
);

const AppContainer = createAppContainer(AppNavigator);

class App extends Component {
  componentDidMount() {
    SplashScreen.hide();
  }
  render() {
    return (
      <GlobalContextProvider>
        <AppContainer />
      </GlobalContextProvider>
    );
  }
}

export default App;
