import React, { PureComponent } from 'react';
import { View, StyleSheet } from 'react-native';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';

class CircularProgressComponent extends PureComponent {
  state = {};
  render() {
    const { props } = this;
    const currentShare =
      (props.currentAmount + props.pendingAmount) / props.maxAmount;
    const pendingShare = 0;
    const totalShare = currentShare + pendingShare;
    const whiteCuttingCircleOverlappingStart = totalShare > 0.75;

    const common = StyleSheet.create({
      circle: {
        width: props.global.deviceWidth / 2,
        height: props.global.deviceWidth / 2,
        borderRadius: props.global.deviceWidth / 4,
        borderWidth: 20
      },
      correctCircleOffset: {
        top: -20,
        right: 0,
        left: -20,
        bottom: 0
      }
    });
    const styles = StyleSheet.create({
      greyBackgroundCircle: {
        ...common.circle,
        borderColor: 'lightgrey'
      },
      blueShareCircle: {
        ...common.circle,
        ...common.correctCircleOffset,
        position: 'absolute',
        borderTopColor: currentShare > 0 ? 'rgb(0,206,209)' : 'transparent',
        borderRightColor:
          currentShare > 0.25 ? 'rgb(0,206,209)' : 'transparent',
        borderBottomColor:
          currentShare > 0.5 ? 'rgb(0,206,209)' : 'transparent',
        borderLeftColor: currentShare > 0.75 ? 'rgb(0,206,209)' : 'transparent',

        transform: [{ rotateZ: '45deg' }]
      },
      whiteCuttingCircle: {
        ...common.circle,
        ...common.correctCircleOffset,
        position: 'absolute',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
        borderRightColor: 'transparent',
        borderTopColor: 'lightgrey',
        transform: [{ rotateZ: `${totalShare * 360}deg` }]
      },
      blueBorderCircle: {
        ...common.circle,
        ...common.correctCircleOffset,
        position: 'absolute',
        borderColor: 'transparent',
        borderTopColor: whiteCuttingCircleOverlappingStart
          ? 'rgb(0,206,209)'
          : 'transparent',
        transform: [{ rotateZ: '45deg' }]
      }
    });

    return (
      <View style={styles.greyBackgroundCircle}>
        {/* render circle in .25 steps */}
        <View style={styles.blueShareCircle}>
          {/* cut to exact currentAmount */}
          <View style={styles.whiteCuttingCircle} />
        </View>
        {/* border to whiteCuttingCircle when overlapping with start */}
        <View style={styles.blueBorderCircle} />
      </View>
    );
  }
}

export default withGlobalContext(CircularProgressComponent);
