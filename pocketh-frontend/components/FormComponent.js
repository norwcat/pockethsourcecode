import React, { PureComponent } from 'react';
import { Input, Label, Item, Form, Icon } from 'native-base';
import { StyleSheet } from 'react-native';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';

class FormComponent extends PureComponent {
  state = {
    width: 0,
    labelMarginTop: '-3%',
  };
  componentDidMount() {
    const keyOfCurrentInput = this.props.focus.current;
    const value = this.reference;
    this.props.global.setValue(keyOfCurrentInput, value);
  }
  componentWillUnmount() {
    const keyOfCurrentInput = this.props.focus.current;
    delete this.props.global[keyOfCurrentInput];
  }
  focusNextOrBlur = () => {
    const keyOfCurrentInput = this.props.focus.current;
    const keyOfNextInput = this.props.focus.next;

    //if no next is passed input gets blurred
    keyOfNextInput
      ? this.props.global[keyOfNextInput].wrappedInstance.focus()
      : this.props.global[keyOfCurrentInput].wrappedInstance.blur();
  };
  handleSubmitEditingAndFocusNext = () => {
    this.props.handleSubmitEditing && this.props.handleSubmitEditing();
    this.focusNextOrBlur();
  };
  calculateMarginBottomInput = () => {
    const heightPassed = this.props.height;
    if (
      heightPassed &&
      this.state.width > this.props.global.deviceWidth * 0.8
    ) {
      return;
    } else if (this.state.width > this.props.global.deviceWidth * 0.8) {
      return this.props.global.deviceWidth * 0.06;
    } else {
      return this.props.global.deviceWidth * 0.02;
    }
  };
  render() {
    const { global } = this.props;
    const calculatedMarginBottomInput = this.calculateMarginBottomInput();
    const styles = StyleSheet.create({
      form: {
        width: this.props.width ? this.props.width : '85%',
      },
      item: {
        paddingVertical: '5%',
        backgroundColor: 'rgb(245,245,245)',
        justifyContent: 'center',
        height: this.props.height
          ? this.props.height
          : global.deviceWidth * 0.15,
      },
      icon: {
        fontSize: 25,
        color: 'grey',
        marginTop: '-5%',
        marginLeft: 10,
        marginRight: -20,
      },
      label: {
        color: 'grey',
        marginLeft: global.deviceWidth * 0.05,
        marginTop: this.state.labelMarginTop,
      },
      input: {
        fontSize: this.props.fontSize ? this.props.fontSize : 18,
        color: this.props.fontColor ? this.props.fontColor : 'black',
        height: this.props.height ? this.props.height : 32,
        marginLeft: global.deviceWidth * 0.04,
      },
    });

    return (
      <Form
        onLayout={(event) => {
          const { width } = event.nativeEvent.layout;
          this.setState({ width });
        }}
        style={styles.form}
        onFocus={() => (
          this.setState({ labelMarginTop: '1%' }),
          console.log(this.state.labelMarginTop)
        )}
        onBlur={() => (
          this.setState({ labelMarginTop: '-3%' }),
          console.log(this.state.labelMarginTop)
        )}
      >
        <Item floatingLabel rounded style={styles.item}>
          <Icon active name={this.props.icon} style={styles.icon} />
          <Label style={styles.label}>{this.props.label}</Label>
          <Input
            getRef={(reference) => {
              this.reference = reference;
            }}
            secureTextEntry={
              this.props.secureInput ? this.props.secureInput : false
            }
            style={styles.input}
            onChangeText={this.props.handleChangeText}
            onFocus={this.props.handleFocus}
            onBlur={this.props.handleBlur}
            value={this.props.value}
            multiline={this.props.multiline}
            keyboardType={this.props.keyboardType}
            autoCorrect={false}
            onSubmitEditing={this.handleSubmitEditingAndFocusNext}
            returnKeyType={
              this.props.returnKeyType ? this.props.returnKeyType : 'next'
            }
          />
        </Item>
      </Form>
    );
  }
}
export default withGlobalContext(FormComponent);
