import React, { PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';
import LocationPreviewComponentIconsDefault from './LocationPreviewComponentIconsDefault';
import LocationPreviewComponentIconsLocation from './LocationPreviewComponentIconsLocation';
import { Badge } from 'native-base';

class LocationPreviewComponent extends PureComponent {
  render() {
    const { navigation } = this.props;
    const imageBackground = require('../assets/card_sbp.png');

    routeEquals = route => {
      try {
        const currentRouteEqualsRoute = navigation.state.routeName === route;
        return currentRouteEqualsRoute;
      } catch (e) {
        console.log(e);
      } finally {
        true;
      }
    };
    const onlyChangeActiveOpacityOnHomeScreen = routeEquals('Location')
      ? 1
      : 0.2;
    const styles = StyleSheet.create({
      imageBackground: { resizeMode: 'contain', width: '100%', height: '100%' },
      mainView: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
      },
      titleTextView: {
        flexDirection: 'row',
        textAlign: 'center',
        paddingLeft: '14%'
      },
      titleText: { color: 'white', fontSize: 20 },
      badge: { marginLeft: '5%' },
      numberOfNotifications: { color: 'white' },
      location: { color: 'white', fontSize: 13, height: '15%' },
      openOrClosed: {
        color: 'white',
        fontSize: 13,
        fontWeight: 'bold',
        height: '15%'
      },
      GuestfundingOrNot: { color: 'white', fontSize: 15, height: '15%' }
    });
    class LocationPreviewComponentIconsDefaultOrLocation extends PureComponent {
      render() {
        {
          if (routeEquals('Location')) {
            return (
              <LocationPreviewComponentIconsLocation navigation={navigation} />
            );
          } else {
            return <LocationPreviewComponentIconsDefault />;
          }
        }
      }
    }
    navigate = screen => {
      navigation.navigate(screen);
    };
    return (
      <TouchableOpacity
        activeOpacity={onlyChangeActiveOpacityOnHomeScreen}
        onPress={() => navigate('Location')}
      >
        <ImageBackground
          source={imageBackground}
          style={styles.imageBackground}
        >
          <View style={styles.mainView}>
            <View style={styles.titleTextView}>
              <Text style={styles.titleText}>State Bird Provisions</Text>
              <Badge danger style={styles.badge}>
                <Text style={styles.numberOfNotifications}>1</Text>
              </Badge>
            </View>
            <Text style={styles.location}>SF, CA</Text>
            <Text style={styles.openOrClosed}>OPEN</Text>
            <Text style={styles.GuestfundingOrNot}>GUESTFUNDING</Text>
            <LocationPreviewComponentIconsDefaultOrLocation />
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

export default withGlobalContext(LocationPreviewComponent);
