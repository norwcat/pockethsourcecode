import React, { PureComponent } from 'react';
import { StyleSheet, Image } from 'react-native';
import { Icon } from 'native-base';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';

class MapComponent extends PureComponent {
  state = {
    region: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    },
    marker: {
      marker1: { latitude: 37.78825, longitude: -122.4324 },
      marker2: { latitude: 37.75825, longitude: -122.4124 },
      marker3: { latitude: 37.76825, longitude: -122.4524 }
    }
  };
  render() {
    const industriesGif = require('../assets/industries.gif');
    const styles = StyleSheet.create({
      mapView: { width: '100%', height: '105%' },
      markerIcon: {
        transform: [{ rotate: '90deg' }],
        bottom: -20,
        right: 4,
        fontSize: 55,
        color: 'darkturquoise',
        marginHorizontal: 10
      },
      markerIconImage: {
        position: 'absolute',
        borderColor: 'darkturquoise',
        borderWidth: 5,
        borderRadius: 25,
        width: 45,
        height: 45
      }
    });
    return (
      <MapView
        initialRegion={this.state.region}
        style={styles.mapView}
        provider={PROVIDER_GOOGLE}
        showsUserLocation={true}
      >
        <Marker coordinate={this.state.marker.marker1}>
          <Icon name="play" style={styles.markerIcon} />
          <Image source={industriesGif} style={styles.markerIconImage} />
        </Marker>
        <Marker coordinate={this.state.marker.marker2}>
          <Icon name="play" style={styles.markerIcon} />
          <Image source={industriesGif} style={styles.markerIconImage} />
        </Marker>
        <Marker coordinate={this.state.marker.marker3}>
          <Icon name="play" style={styles.markerIcon} />
          <Image source={industriesGif} style={styles.markerIconImage} />
        </Marker>
      </MapView>
    );
  }
}
export default MapComponent;
