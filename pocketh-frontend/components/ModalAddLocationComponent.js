import React, { PureComponent } from 'react';
import {
  Text,
  Modal,
  Animated,
  Easing,
  TextInput,
  StyleSheet
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Container, Button, Footer, FooterTab } from 'native-base';
import { Col, Grid, Row } from 'react-native-easy-grid';
import GestureRecognizer from 'react-native-swipe-gestures';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';

class ModalAddLocationComponent extends PureComponent {
  state = {
    backgroundEaseIn: new Animated.Value(0),
    xPositionModal: new Animated.Value(-500),
    xPositionPayNow: new Animated.Value(-500),
    region: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    },
    tokensAmount: undefined,
    emailOfUSer: undefined
  };
  componentDidMount() {
    Animated.parallel([
      Animated.timing(this.state.backgroundEaseIn, {
        toValue: 1,
        easing: Easing.inOut(Easing.ease),
        duration: 500
      }),
      Animated.timing(this.state.xPositionModal, {
        toValue: 0,
        easing: Easing.inOut(Easing.ease),
        duration: 300
      }),
      Animated.timing(this.state.xPositionPayNow, {
        toValue: 0,
        easing: Easing.inOut(Easing.ease),
        duration: 150
      })
    ]).start();
  }

  render() {
    const { global } = this.props;
    modalEaseOut = () => {
      Animated.parallel([
        Animated.timing(this.state.backgroundEaseIn, {
          toValue: 0,
          easing: Easing.inOut(Easing.ease),
          duration: 200
        }),
        Animated.timing(this.state.xPositionModal, {
          toValue: -this.props.global.deviceHeight * 1,
          easing: Easing.inOut(Easing.ease),
          duration: 200
        }),
        Animated.timing(this.state.xPositionPayNow, {
          toValue: -this.props.global.deviceHeight * 0.5,
          easing: Easing.inOut(Easing.ease),
          duration: 600
        })
      ]).start();
    };
    toggleModal = () => {
      //disable modal after animation finished
      setTimeout(() => {
        this.props.toggleAddLocationModal('isModalVisibleAddLocation');
      }, 300);
    };
    doInputTextChange = (input, key) => {
      this.setState({ [key]: input });
    };
    modalAvoidKeyBoard = () => {
      this.setState({
        xPositionModal: new Animated.Value(global.deviceHeight * 0.2)
      });
    };
    modalResetPosition = () => {
      this.setState({
        xPositionModal: new Animated.Value(0)
      });
    };
    const styles = StyleSheet.create({
      animatedViewMap: {
        marginTop:
          global.devicePixelRatio > 2
            ? global.deviceHeight * 0.053
            : global.deviceHeight * 0.03,
        marginBottom:
          global.devicePixelRatio > 2
            ? -global.deviceHeight * 0.065
            : -global.deviceHeight * 0.04,
        backgroundColor: 'rgb(255,255,255)',
        height: global.deviceHeight,
        width: global.deviceWidth,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        borderColor: 'white',
        overflow: 'hidden'
      },
      mapView: {
        flex: 1
      },
      animatedViewInput: {
        position: 'absolute',
        right: 0,
        left: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        height: global.deviceHeight / 3,
        width: global.deviceWidth,
        marginBottom: '16%'
      },
      grid: {
        marginTop: '5%',
        marginRight: '5%',
        marginBottom: '25%',
        marginLeft: '5%'
      },
      textInput: {
        fontSize: 18,
        backgroundColor: 'whitesmoke',
        borderWidth: 5,
        borderColor: 'whitesmoke',
        borderRadius: 10,
        marginTop: -4,
        textAlign: 'center',
        justifyContent: 'center'
      },
      animatedViewAddLocationButton: {
        position: 'absolute',
        right: 0,
        left: 0
      },
      footer: {
        borderTopWidth: 0,
        backgroundColor: 'white'
      },
      addLocationButton: {
        marginVertical: global.deviceHeight * 0.05,
        alignSelf: 'center',
        height: global.deviceHeight * 0.12,
        borderTopColor: 'white',
        borderTopWidth: 15
      },
      addLocationButtonText: {
        color: 'white',
        fontWeight: 'bold',
        marginBottom: global.deviceHeight * 0.03
      },
      modalText: {
        color: 'rgb(100,100,100)',
        fontSize: 18
      }
    });

    return (
      <GestureRecognizer
        onSwipeDown={state => {
          modalEaseOut();
          toggleModal();
        }}
        config={{
          velocityThreshold: 0.05,
          directionalOffsetThreshold: 80,
          gestureIsClickThreshold: 32
        }}
      >
        <Modal
          animationType="none"
          transparent={true}
          onRequestClose={this.closeModal}
        >
          <Animated.View
            style={{
              ...styles.animatedViewMap,
              opacity: this.state.backgroundEaseIn
            }}
          >
            <MapView
              style={styles.mapView}
              provider={PROVIDER_GOOGLE}
              initialRegion={this.state.region}
              showsUserLocation={true}
            />
          </Animated.View>
          <Animated.View
            style={{
              ...styles.animatedViewInput,
              bottom: this.state.xPositionModal
            }}
          >
            <Grid style={styles.grid}>
              <Row>
                <Col>
                  <Text style={styles.modalText}>Name of location</Text>
                </Col>
                <Col>
                  <TextInput
                    returnKeyType="next"
                    label=" "
                    keyboardType="default"
                    onChangeText={input =>
                      doInputTextChange(input, 'nameOfLocation')
                    } //maybe use react hook here
                    value={this.state.nameOfLocation}
                    onFocus={() => modalAvoidKeyBoard()}
                    onBlur={() => modalResetPosition()}
                    style={styles.textInput}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={styles.modalText}>State</Text>
                </Col>
                <Col>
                  <TextInput
                    returnKeyType="done"
                    label=" "
                    keyboardType="default"
                    onChangeText={input => doInputTextChange(input, 'state')} //maybe use react hook here
                    value={this.state.state}
                    onFocus={() => modalAvoidKeyBoard()}
                    onBlur={() => modalResetPosition()}
                    style={styles.textInput}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={styles.modalText}>City</Text>
                </Col>
                <Col>
                  <TextInput
                    returnKeyType="done"
                    label=" "
                    keyboardType="default"
                    onChangeText={input => doInputTextChange(input, 'city')} //maybe use react hook here
                    value={this.state.city}
                    onFocus={() => modalAvoidKeyBoard()}
                    onBlur={() => modalResetPosition()}
                    style={styles.textInput}
                  />
                </Col>
              </Row>
            </Grid>
          </Animated.View>
          <Container>
            <Animated.View
              style={{
                ...styles.animatedViewAddLocationButton,
                bottom: this.state.xPositionPayNow
              }}
            >
              <Footer style={styles.footer}>
                <FooterTab>
                  <Button
                    info
                    full
                    style={styles.addLocationButton}
                    onPress={() => {
                      //prevent animation from lagging
                      setTimeout(() => {
                        modalEaseOut();
                        toggleModal();
                      }, 100);
                    }}
                  >
                    <Text style={styles.addLocationButtonText}>
                      ADD LOCATION NOW
                    </Text>
                  </Button>
                </FooterTab>
              </Footer>
            </Animated.View>
          </Container>
        </Modal>
      </GestureRecognizer>
    );
  }
}

export default withGlobalContext(ModalAddLocationComponent);
