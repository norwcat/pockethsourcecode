import React, { Component } from 'react';
import { Dimensions } from 'react-native';

export const GlobalContext = React.createContext({});

export class GlobalContextProvider extends Component {
  state = {
    deviceHeight: Dimensions.get('window').height,
    deviceWidth: Dimensions.get('window').width,
    devicePixelRatio:
      Dimensions.get('window').height / Dimensions.get('window').width,
    username: '',
    password: '',
    currentTokens: 125,
    pendingTokens: 25,
    maxTokens: 250,
    qrData: { amount: undefined, to: undefined },
    isBarCodeReaderEnabled: true,
    modalVisible: false,
    useTokens: true,
    isDrawerOpen: false,
    isVisibleHowToAddLocationInfo: false,
    setValue: (key, value) => {
      this.setState({ [key]: value });
    },
    setKeyValue: (key, value) => {
      this.setState({ [key]: value });
    }
  };

  render() {
    return (
      <GlobalContext.Provider value={{ ...this.state }}>
        {this.props.children}
      </GlobalContext.Provider>
    );
  }
}

export const withGlobalContext = ChildComponent => props => (
  <GlobalContext.Consumer>
    {context => <ChildComponent {...props} global={context} />}
  </GlobalContext.Consumer>
);
