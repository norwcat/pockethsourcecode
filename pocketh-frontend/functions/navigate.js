export default (navigate = (route, navigation, global) => {
  navigation.navigate(route);
  global.setValue('currentScreen', route);
  console.log(`navigating to ${route}`);
});
