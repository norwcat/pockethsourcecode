import React, { Component } from 'react';
import { StyleSheet, SafeAreaView, Image } from 'react-native';
import { Container, Content, Text, Button } from 'native-base';
import OwnHeaderComponent from '../components/OwnHeaderComponent';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';

class EnableLocationScreen extends Component {
  render() {
    const { navigation, global } = this.props;
    const imageSource = require('../assets/intromapwithicons.png');
    const styles = StyleSheet.create({
      safeAreaView: {
        alignContent: 'center',
        height: global.deviceHeight
      },
      text: {
        color: 'purple',
        fontSize: 24,
        fontWeight: '200',
        textAlign: 'center',
        marginTop: '20%'
      },
      image: {
        resizeMode: 'contain',
        width: global.deviceWidth * 0.7,
        height: '50%',
        alignSelf: 'center'
      },
      button: {
        position: 'absolute',
        bottom: global.deviceHeight * 0.2,
        alignSelf: 'center',
        width: '85%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#28cace'
      },
      textButton: {
        color: 'white',
        fontSize: 18
      }
    });
    return (
      <Container>
        <OwnHeaderComponent navigation={navigation} />
        <Content>
          <SafeAreaView style={styles.safeAreaView}>
            <Text style={styles.text}>
              Never miss a {'\n'} participating location
            </Text>
            <Image source={imageSource} style={styles.image} />
            <Button
              info
              style={styles.button}
              onPress={() => {
                navigation.navigate('EnableNotifications');
              }}
            >
              <Text style={styles.textButton}>ENABLE LOCATION</Text>
            </Button>
          </SafeAreaView>
        </Content>
      </Container>
    );
  }
}

export default withGlobalContext(EnableLocationScreen);
