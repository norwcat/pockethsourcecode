import React, { PureComponent } from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Text,
  Image,
  View,
} from 'react-native';
import { Grid, Row, Col, Button } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';

class IntroScreen extends PureComponent {
  state = {
    currentPage: 0,
  };

  render() {
    const { global, navigation } = this.props;
    const images = {
      first: require('../assets/receipt.png'),
      second: require('../assets/stores.png'),
      third: {
        scanAndPay: require('../assets/scanNpay.png'),
        yoga: require('../assets/yoga.png'),
        workout: require('../assets/workout.png'),
        coffee: require('../assets/coffee.png'),
        movies: require('../assets/movies.png'),
      },
      fourth: require('../assets/intromapwithicons.png'),
      fifth: {
        storesWithPocketh: require('../assets/storeswithpocketh.png'),
        industries: require('../assets/industries.gif'),
      },
    };

    handleScroll = (event) => {
      this.setState({
        currentPage: Math.round(
          event.nativeEvent.contentOffset.x / this.props.global.deviceWidth
        ),
      });
    };

    const styles = StyleSheet.create({
      scrollViewContentContainer: {
        paddingTop: global.deviceHeight * 0.09,
      },
      safeAreaView: { width: global.deviceWidth, height: global.deviceHeight },
      text: {
        fontSize: 23,
        textAlign: 'center',
        color: 'white',
      },
      textPocketh: {
        fontSize: 69 + 42.0 - 13,
        textAlign: 'center',
        color: 'white',
        fontWeight: '100',
      },
      imageFirst: {
        width: global.deviceWidth * 0.666,
        height: global.deviceWidth * 0.666,
        alignSelf: 'center',
        marginTop:
          global.devicePixelRatio > 2
            ? global.deviceHeight * 0.12
            : global.deviceHeight * 0.09,
      },
      imageSecond: {
        width: global.deviceWidth * 0.9,
        height: global.deviceWidth * 0.666,
        alignSelf: 'center',
        marginTop:
          global.devicePixelRatio > 2
            ? global.deviceHeight * 0.12
            : global.deviceHeight * 0.07,

        resizeMode: 'contain',
      },
      rowImagesThirdSmall: {
        position: 'absolute',
        top: global.deviceHeight * 0.1,
        width: global.deviceWidth,
        alignContent: 'center',
        justifyContent: 'space-evenly',
      },
      imageThirdSmall: {
        width: global.deviceWidth * 0.12,
        height: global.deviceWidth * 0.13,
      },
      imageThird: {
        width: global.deviceWidth * 1,
        height: global.deviceWidth * 1,
        alignSelf: 'center',
        marginTop:
          global.devicePixelRatio > 2
            ? global.deviceHeight * 0.15
            : global.deviceHeight * 0.1,
      },
      imageFourth: {
        width: global.deviceWidth * 0.666,
        height: global.deviceWidth * 0.666,
        alignSelf: 'center',
        marginTop:
          global.devicePixelRatio > 2
            ? global.deviceHeight * 0.08
            : global.deviceHeight * 0.04,
      },
      imageFifth: {
        width: global.deviceWidth * 0.9,
        height: global.deviceWidth * 0.666,
        alignSelf: 'center',
        marginTop:
          global.devicePixelRatio > 2
            ? global.deviceHeight * 0.08
            : global.deviceHeight * 0.03,

        resizeMode: 'contain',
      },
      imageFifthSmall: {
        top: -global.deviceHeight * 0.365,
        left: global.deviceWidth * 0.0,

        width: global.deviceWidth * 0.15,
        alignSelf: 'center',
        marginTop: global.devicePixelRatio > 2 ? '30%' : '18%',
        resizeMode: 'contain',
      },
      safeAreaViewBottom: {
        position: 'absolute',
        top: global.deviceHeight * 0.7,
        width: global.deviceWidth,
        alignContent: 'center',
      },
      gridCurrentSitePoint: { width: '33%', alignSelf: 'center' },
      colCurrentSitePoint: { alignItems: 'center' },
      viewCurrentSitePoint: {
        borderRadius: 3.5,
        borderWidth: 3.5,
        width: 7,
      },
      textNoAccountYet: { fontSize: 21, textAlign: 'center', color: 'white' },
      textSignUpAndCheckItOut: {
        fontSize: 16,
        textAlign: 'center',
        color: 'white',
        marginTop: '5%',
      },
      button: {
        alignSelf: 'center',
        width: '100%',
        height: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(40, 202, 206,0.5)',
        paddingBottom: '30%',
        paddingTop: '5%',
        marginTop: '15%',
      },
      colButton: {
        alignItems: 'center',
      },
      colButtonHitSlop: {
        top: global.deviceHeight * 0.03,
        bottom: global.deviceHeight * 0.1,
      },
      textButton: { color: 'white', fontSize: 18 },
      viewBottomSpacing: {
        position: 'absolute',
        top: 0,
        right: global.deviceWidth * 0.5,
        height: '320%',
        borderRightWidth: 0.2,
        borderRightColor: 'rgba(250,250,250,0.8)',
      },
    });

    return (
      <LinearGradient
        colors={['rgb(84,174,174)', 'rgb(97,177,158)', 'rgb(110,179,142)']}
      >
        <ScrollView
          horizontal={true}
          pagingEnabled={true}
          contentContainerStyle={styles.scrollViewContentContainer}
          showsHorizontalScrollIndicator={false}
          onScroll={handleScroll}
        >
          <SafeAreaView style={styles.safeAreaView}>
            <Text style={styles.text}>
              Pay & earn instant cash-like tokens {'\n'}
              from your favorite places
            </Text>
            <Image source={images.first} style={styles.imageFirst} />
          </SafeAreaView>
          <SafeAreaView style={styles.safeAreaView}>
            <Text style={styles.text}>
              With guestfunding help your {'\n'}
              community grow & get {'\n'}
              rewarded for it
            </Text>
            <Image source={images.second} style={styles.imageSecond} />
          </SafeAreaView>
          <SafeAreaView style={styles.safeAreaView}>
            <Text style={styles.text}>{'\n'}No more limits to redeem!!</Text>
            <Row style={styles.rowImagesThirdSmall}>
              <Image
                source={images.third.yoga}
                style={styles.imageThirdSmall}
              />
              <Image
                source={images.third.workout}
                style={styles.imageThirdSmall}
              />
              <Image
                source={images.third.coffee}
                style={styles.imageThirdSmall}
              />
              <Image
                source={images.third.movies}
                style={styles.imageThirdSmall}
              />
            </Row>
            <Image source={images.third.scanAndPay} style={styles.imageThird} />
          </SafeAreaView>
          <SafeAreaView style={styles.safeAreaView}>
            <Text style={styles.text}>
              {'\n'}Use Pocketh all over town{'\n'}
            </Text>
            <Image source={images.fourth} style={styles.imageFourth} />
          </SafeAreaView>
          <SafeAreaView style={styles.safeAreaView}>
            <Text style={styles.textPocketh}>Pocketh</Text>
            <Text style={styles.text}>Be a part of what you ❤️</Text>
            <View>
              <Image
                source={images.fifth.storesWithPocketh}
                style={styles.imageFifth}
              />

              <Image
                source={images.fifth.industries}
                style={styles.imageFifthSmall}
              />
            </View>
          </SafeAreaView>
        </ScrollView>
        <SafeAreaView style={styles.safeAreaViewBottom}>
          <Grid style={styles.gridCurrentSitePoint}>
            <Col style={styles.colCurrentSitePoint}>
              <View
                style={{
                  ...styles.viewCurrentSitePoint,
                  borderColor: this.state.currentPage === 0 ? 'white' : 'grey',
                }}
              />
            </Col>
            <Col style={styles.colCurrentSitePoint}>
              <View
                style={{
                  ...styles.viewCurrentSitePoint,
                  borderColor: this.state.currentPage === 1 ? 'white' : 'grey',
                }}
              />
            </Col>
            <Col style={styles.colCurrentSitePoint}>
              <View
                style={{
                  ...styles.viewCurrentSitePoint,
                  borderColor: this.state.currentPage === 2 ? 'white' : 'grey',
                }}
              />
            </Col>
            <Col style={styles.colCurrentSitePoint}>
              <View
                style={{
                  ...styles.viewCurrentSitePoint,
                  borderColor: this.state.currentPage === 3 ? 'white' : 'grey',
                }}
              />
            </Col>
            <Col style={styles.colCurrentSitePoint}>
              <View
                style={{
                  ...styles.viewCurrentSitePoint,
                  borderColor: this.state.currentPage === 4 ? 'white' : 'grey',
                }}
              />
            </Col>
          </Grid>
          <Text
            style={styles.textNoAccountYet}
            onPress={() => navigation.navigate('SignUp')}
          >
            No account yet?
          </Text>
          <Text
            style={styles.textSignUpAndCheckItOut}
            onPress={() => navigation.navigate('SignUp')}
          >
            Sign up & check it out!
          </Text>
          <Button style={styles.button}>
            <Grid>
              <Col
                style={styles.colButton}
                onPress={() => {
                  navigation.navigate('Login');
                }}
                hitSlop={styles.colButtonHitSlop}
              >
                <Text style={styles.textButton}>Login</Text>
              </Col>
              <Col
                style={styles.colButton}
                onPress={() => {
                  navigation.navigate('SignUp');
                }}
                hitSlop={styles.colButtonHitSlop}
              >
                <Text style={styles.textButton}>Sign Up!</Text>
              </Col>
            </Grid>
            <View style={styles.viewBottomSpacing} />
          </Button>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

export default withGlobalContext(IntroScreen);
