import React, { PureComponent } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Grid, Col, Row, Container, Content } from 'native-base';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';
import OwnHeaderComponent from '../components/OwnHeaderComponent';
import BeautifulButtonComponent from '../components/BeautifulButtonComponent';
import MountModalsBuyPayGiftComponent from '../components/MountModalsBuyPayGiftComponent';
import OwnFooterComponent from '../components/OwnFooterComponent';
import LocationPreviewComponent from '../components/LocationPreviewComponent';
import CircularProgressComponent from '../components/CircularProgressComponent';

class LocationScreen extends PureComponent {
  state = {
    isModalVisibleBuy: false,
    isModalVisiblePay: false,
    isModalVisibleGift: false
  };

  render() {
    const { global, navigation } = this.props;
    const styles = StyleSheet.create({
      viewLocationPreviewComponent: {
        width: '100%',
        height: global.deviceHeight / 4
      },
      viewGuestfunding: { alignItems: 'center' },
      textGuestfunding: {
        fontSize: 32,
        lineHeight: 75,
        color: 'rgb(60,60,60)'
      },
      viewBoughtTokens: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '10%'
      },
      textBoughtTokens: {
        textAlign: 'left',
        alignSelf: 'flex-start',
        marginTop: '68%',
        paddingLeft: '20%',
        fontSize: 18,
        color: 'teal'
      }
    });
    toggleModal = modal => {
      const isModalVisible = !this.state[modal];
      this.setState({ [modal]: isModalVisible });
    };

    return (
      <Container>
        <OwnHeaderComponent navigation={navigation} />
        <Content>
          <MountModalsBuyPayGiftComponent
            isModalVisible={{
              buy: this.state.isModalVisibleBuy,
              pay: this.state.isModalVisiblePay,
              gift: this.state.isModalVisibleGift
            }}
            toggleModal={toggleModal}
            navigation={navigation}
          />
          <View style={styles.viewLocationPreviewComponent}>
            <LocationPreviewComponent navigation={navigation} />
          </View>
          <View>
            <Grid>
              <Row>
                <Col>
                  <BeautifulButtonComponent
                    name={'BUY'}
                    width={70}
                    onPress={() => toggleModal('isModalVisibleBuy')}
                  />
                </Col>
                <Col>
                  <BeautifulButtonComponent
                    name={'GIFT'}
                    width={70}
                    onPress={() => toggleModal('isModalVisibleGift')}
                  />
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={styles.viewGuestfunding}>
            <Text style={styles.textGuestfunding}>Guestfunding</Text>
          </View>
          <View style={styles.viewBoughtTokens}>
            <Grid>
              <Col size={1}>
                <Text style={styles.textBoughtTokens}>
                  Bought {'\n'} {global.pendingTokens + global.currentTokens}{' '}
                  {'\n'}tokens
                </Text>
              </Col>
              <Col size={3}>
                <CircularProgressComponent
                  currentAmount={global.currentTokens}
                  pendingAmount={global.pendingTokens}
                  maxAmount={global.maxTokens}
                  global={global}
                />
              </Col>
            </Grid>
          </View>
        </Content>
        <OwnFooterComponent global={global} toggleModal={toggleModal} />
      </Container>
    );
  }
}

export default withGlobalContext(LocationScreen);
