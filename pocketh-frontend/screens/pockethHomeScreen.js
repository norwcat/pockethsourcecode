import React, { PureComponent } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Container, Content, Icon, Col, Grid } from 'native-base';
import { Overlay } from 'react-native-elements';
import SideMenu from 'react-native-side-menu';
import MapComponent from '../components/MapComponent';
import TokensComponent from '../components/TokensComponent';
import BeautifulButtonComponent from '../components/BeautifulButtonComponent';
import TransactionsComponent from '../components/TransactionsComponent';
import MountModalsBuyPayGiftComponent from '../components/MountModalsBuyPayGiftComponent';
import MountModalAddLocationComponent from '../components/MountModalAddLocationComponent';
import LocationPreviewComponent from '../components/LocationPreviewComponent';
import OwnHeaderComponent from '../components/OwnHeaderComponent';
import OwnFooterComponent from '../components/OwnFooterComponent';
import MainMenuDrawer from '../components/MainMenuDrawerComponent';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';

class HomeScreen extends PureComponent {
  state = {
    transactions: [
      [
        ['10/29/18', '8:02PM'],
        ['Paid'],
        ['StateBirdProvisions'],
        ['-50', 'USD']
      ]
    ],
    categories: ['date', 'transactions', 'to/from', 'amount'],
    transactionsNew: [
      {
        date: '10/29/18',
        time: '8:02PM',
        status: 'Paid',
        location: 'StateBirdProvisions',
        amount: '-50',
        currency: 'USD'
      },
      {
        date: '10/29/18',
        time: '8:02PM',
        status: 'Paid',
        location: 'StateBirdProvisions',
        amount: '-50',
        currency: 'USD'
      },
      {
        date: '10/29/18',
        time: '8:02PM',
        status: 'Paid',
        location: 'StateBirdProvisions',
        amount: '-50',
        currency: 'USD'
      },
      {
        date: '10/29/18',
        time: '8:02PM',
        status: 'Paid',
        location: 'StateBirdProvisions',
        amount: '-50',
        currency: 'USD'
      },
      {
        date: '10/29/18',
        time: '8:02PM',
        status: 'Paid',
        location: 'StateBirdProvisions',
        amount: '-50',
        currency: 'USD'
      }
    ],
    categoriesNew: {
      date: 'date',
      transaction: 'transactions',
      to_from: 'to/from',
      amount: 'amount'
    },
    showTransactionHistory: false,
    isModalVisibleBuy: false,
    isModalVisiblePay: false,
    isModalVisibleGift: false,
    isModalVisibleAddLocation: false,
    isDrawerOpen: false
  };

  render() {
    const { navigation, global } = this.props;
    const styles = StyleSheet.create({
      viewTransactionsComponent: {
        width: global.deviceWidth,
        paddingBottom: global.deviceHeight * 0.03
      },
      viewTransactionsComponentEmpty: {
        paddingTop: global.deviceHeight * 0.03
      },
      viewIntroductionText: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.8)',
        borderRadius: global.deviceHeight * 0.025,
        marginTop: -global.deviceHeight * 0.012,
        marginBottom: global.deviceHeight * 0.012
      },
      textIntroduction: {
        margin: global.deviceHeight * 0.025,
        textAlign: 'center',
        fontSize: global.devicePixelRatio > 2 ? 12.5 : 14
      },
      viewButtonAddLocationIntroduction: {
        marginTop:
          global.devicePixelRatio > 2
            ? global.deviceHeight * 0.05
            : global.deviceHeight * 0.025
      },
      textRemindMeLater: {
        color: 'white',
        textAlign: 'center',
        marginTop: global.deviceHeight * 0.08,
        fontSize: 16
      },
      content: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
      },
      viewTokensComponent: { marginTop: '5%' },
      grid: {
        height: global.deviceHeight / 18
      },
      col: { justifyContent: 'center', alignItems: 'center' },
      textCol: {
        color: 'teal',
        fontWeight: '400',
        fontSize: 14
      },
      iconCol: { fontSize: 18, color: 'teal' },
      viewLocationPreviewComponent: {
        width: '100%',
        height: global.deviceHeight * 0.2
      },
      viewButtonAddLocation: {
        alignContent: 'center'
      },
      viewMapComponent: {
        width: '100%',
        height: global.deviceHeight * 0.48,
        marginBottom: global.deviceHeight * 0.02
      },
      lightTitleTeal: {
        fontSize: 40,
        fontWeight: '200',
        lineHeight: 75,
        color: 'rgb(60,60,60)'
      }
    });

    toggleModal = modal => {
      const isModalVisible = !this.state[modal];
      this.setState({ [modal]: isModalVisible });
    };
    toggleDrawer = isDrawerOpen => {
      this.setState({ isDrawerOpen });
    };
    const RenderTransactionsComponent = () => {
      if (this.state.showTransactionHistory === true) {
        return (
          <View style={styles.viewTransactionsComponent}>
            <TransactionsComponent
              transactions={this.state.transactions}
              categories={this.state.categories}
              transactionsNew={this.state.transactionsNew}
              categoriesNew={this.state.categoriesNew}
              showTransactionHistory={this.state.showTransactionHistory}
              {...this.props}
            />
            <BeautifulButtonComponent
              name={'SEE ALL'}
              onPress={() =>
                this.props.navigation.navigate('TransactionHistory')
              }
            />
          </View>
        );
      }
      return <View style={styles.viewTransactionsComponentEmpty} />;
    };
    return (
      <SideMenu
        menu={
          <MainMenuDrawer navigation={navigation} toggleDrawer={toggleDrawer} />
        }
        disableGestures={true}
        isOpen={this.state.isDrawerOpen}
        onChange={isOpen => (isOpen === true ? null : toggleDrawer(false))}
      >
        <Overlay
          isVisible={global.isVisibleHowToAddLocationInfo}
          onBackdropPress={() =>
            global.setKeyValue('isVisibleHowToAddLocationInfo', false)
          }
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="transparent"
          width={global.deviceWidth * 0.66}
          height={global.deviceHeight * 0.197}
        >
          <View style={styles.viewIntroductionText}>
            <Text style={styles.textIntroduction}>
              Tap on 'Add Location' and incentivize your favorite place not yet
              on Pocketh to become part of us by tipping the 10$ you just
              received for signing up
            </Text>
            <View style={styles.viewButtonAddLocationIntroduction}>
              <BeautifulButtonComponent
                name={'add location'}
                onPress={() => {
                  global.setKeyValue('isVisibleHowToAddLocationInfo', false);
                  toggleModal('isModalVisibleAddLocation');
                }}
                backgroundColor="white"
              />
            </View>
            <TouchableOpacity
              onPress={() =>
                this.setState({ isVisibleHowToAddLocationInfo: false })
              }
            >
              <Text style={styles.textRemindMeLater}>Remind me later!</Text>
            </TouchableOpacity>
          </View>
        </Overlay>
        <Container>
          <MountModalAddLocationComponent
            isModalVisible={this.state.isModalVisibleAddLocation}
            toggleModal={toggleModal}
          />
          <MountModalsBuyPayGiftComponent
            isModalVisible={{
              buy: this.state.isModalVisibleBuy,
              pay: this.state.isModalVisiblePay,
              gift: this.state.isModalVisibleGift
            }}
            toggleModal={toggleModal}
            navigation={navigation}
          />
          <OwnHeaderComponent
            toggleDrawer={toggleDrawer}
            navigation={navigation}
          />
          <Content contentContainerStyle={styles.content}>
            <View style={styles.viewTokensComponent}>
              <TokensComponent
                tokens={this.props.global.currentTokens}
                tokensPending={this.props.global.pendingTokens}
              />
            </View>
            <BeautifulButtonComponent
              name={'transaction history'}
              onPress={() =>
                this.setState({
                  showTransactionHistory: !this.state.showTransactionHistory
                })
              }
            />
            <RenderTransactionsComponent />
            <Grid style={styles.grid}>
              <Col style={styles.col} onPress={() => navigation.goBack()}>
                <Text style={styles.textCol}>
                  NEARBY <Icon name="arrow-down" style={styles.iconCol} />
                </Text>
              </Col>
              <Col style={styles.col} onPress={() => navigation.goBack()}>
                <Text style={styles.textCol}>
                  TYPE <Icon name="arrow-down" style={styles.iconCol} />
                </Text>
              </Col>
              <Col style={styles.col} onPress={() => navigation.goBack()}>
                <Icon name="search" style={styles.iconCol} />
              </Col>
            </Grid>
            <View style={styles.viewLocationPreviewComponent}>
              <LocationPreviewComponent navigation={navigation} />
            </View>
            <View style={styles.viewButtonAddLocation}>
              <BeautifulButtonComponent
                name={'add location'}
                onPress={() => toggleModal('isModalVisibleAddLocation')}
              />
            </View>
            <Text style={styles.lightTitleTeal}>My Community</Text>
            <Grid style={styles.grid}>
              <Col style={styles.col} onPress={() => navigation.goBack()}>
                <Text style={styles.textCol}>
                  NEARBY <Icon name="arrow-down" style={styles.iconCol} />
                </Text>
              </Col>
              <Col />
              <Col style={styles.col} onPress={() => navigation.goBack()}>
                <Icon name="search" style={styles.iconCol} />
              </Col>
            </Grid>
            <View style={styles.viewLocationPreviewComponent}>
              <LocationPreviewComponent navigation={navigation} />
            </View>
            <View>
              <Text style={styles.lightTitleTeal}>Locations</Text>
            </View>
            <View style={styles.viewMapComponent}>
              <MapComponent />
            </View>
          </Content>
          <OwnFooterComponent toggleModal={toggleModal} />
        </Container>
      </SideMenu>
    );
  }
}

export default withGlobalContext(HomeScreen);
