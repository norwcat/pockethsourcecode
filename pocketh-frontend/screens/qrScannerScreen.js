import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Title } from 'native-base';
import { RNCamera } from 'react-native-camera';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';

class QRScannerScreen extends PureComponent {
  extractScannerData = e => {
    const data = e.split('|');
    const qrData = {};
    (qrData.amount = parseInt(data[0])), (qrData.to = data[1]);
    return qrData;
  };

  doBarCodeRead = e => {
    if (this.props.global.isBarCodeReaderEnabled === true) {
      const qrData = this.extractScannerData(e.data);
      this.props.global.setKeyValue('qrData', qrData);
      this.props.global.setKeyValue('isBarCodeReaderEnabled', false);
      this.props.navigation.getParam('toggleModal')();
      this.props.navigation.pop();
    }
  };

  render() {
    const { navigation } = this.props;
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: 'column'
      },
      viewTitle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '5%'
      },
      RNCamera: {
        flex: 5,
        justifyContent: 'flex-end',
        alignItems: 'center'
      },
      viewTouchableOpacity: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
      },
      touchableOpacityCancel: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
      },
      textCancel: { fontSize: 14 }
    });
    return (
      <View style={styles.container}>
        <View style={styles.viewTitle}>
          <Title> Please scan the QR code on your receipt :)</Title>
        </View>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.RNCamera}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          captureAudio={false}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
          barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
          onBarCodeRead={e => this.doBarCodeRead(e)}
        />
        <View style={styles.viewTouchableOpacity}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.touchableOpacityCancel}
          >
            <Text style={styles.textCancel}> CANCEL </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
export default withGlobalContext(QRScannerScreen);
