import React, { PureComponent } from 'react';
import { Container, Content } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { withGlobalContext } from '../contextProviders/GlobalContextProvider';
import OwnHeaderComponent from '../components/OwnHeaderComponent';
import TransactionsComponent from '../components/TransactionsComponent';

class HelpScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      transactions: [
        [
          ['10/29/18', '8:02PM'],
          ['Paid'],
          ['StateBirdProvisions'],
          ['-50', 'USD']
        ]
      ],
      categories: ['date', 'transactions', 'to/from', 'amount'],
      transactionsNew: [
        {
          date: '10/29/18',
          time: '8:02PM',
          status: 'Paid',
          location: 'StateBirdProvisions',
          amount: '-50',
          currency: 'USD'
        },
        {
          date: '10/29/18',
          time: '8:02PM',
          status: 'Paid',
          location: 'StateBirdProvisions',
          amount: '-50',
          currency: 'USD'
        },
        {
          date: '10/29/18',
          time: '8:02PM',
          status: 'Paid',
          location: 'StateBirdProvisions',
          amount: '-50',
          currency: 'USD'
        },
        {
          date: '10/29/18',
          time: '8:02PM',
          status: 'Paid',
          location: 'StateBirdProvisions',
          amount: '-50',
          currency: 'USD'
        },
        {
          date: '10/29/18',
          time: '8:02PM',
          status: 'Paid',
          location: 'StateBirdProvisions',
          amount: '-50',
          currency: 'USD'
        }
      ],
      categoriesNew: {
        date: 'date',
        transaction: 'transactions',
        to_from: 'to/from',
        amount: 'amount'
      },
      showTransactionHistory: true
    };
  }

  render() {
    const { navigation, global } = this.props;
    return (
      <React.Fragment>
        <LinearGradient colors={['#54aeae', '#61b19e', '#6eb38e']}>
          <OwnHeaderComponent navigation={navigation} />
        </LinearGradient>
        <Content>
          <TransactionsComponent
            transactions={this.state.transactions}
            categories={this.state.categories}
            transactionsNew={this.state.transactionsNew}
            categoriesNew={this.state.categoriesNew}
            showTransactionHistory={this.state.showTransactionHistory}
            {...this.props}
          />
        </Content>
      </React.Fragment>
    );
  }
}

export default withGlobalContext(HelpScreen);
