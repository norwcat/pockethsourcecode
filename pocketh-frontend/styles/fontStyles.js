import { material, systemWeights } from 'react-native-typography';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  lightTitle: {
    ...material.display4White,
    ...systemWeights.thin,
    fontSize: 100,
    paddingTop: 100,
    color: 'white'
  },
  lightTitleImageText: {
    ...material.display4White,
    ...systemWeights.light,
    fontSize: 30,
    lineHeight: undefined,
    color: 'white'
  },
  lightTitleTeal: {
    ...material.display4White,
    ...systemWeights.light,
    fontSize: 40,
    lineHeight: 75,
    color: 'rgb(60,60,60)'
  },
  lightToken: {
    ...material.display4White,
    ...systemWeights.light,
    fontSize: 40,
    lineHeight: undefined,
    color: 'teal'
  },
  Subtitle: {
    ...material.headlineWhite,
    ...systemWeights.light,
    fontSize: 20,
    color: 'white',
    //backgroundColor: 'rgba(255, 255, 255, 0.1)',
    flex: 1
  },
  lightSubtitle: {
    ...material.headlineWhite,
    ...systemWeights.thin,
    fontSize: 20,
    paddingBottom: 60,
    color: 'white'
  },
  lightSubtitleBig: {
    ...material.headlineWhite,
    ...systemWeights.thin,
    fontSize: 50,
    paddingTop: '15%',
    paddingLeft: '7%',
    color: 'white'
  },
  lightButton: {
    ...material.headlineWhite,
    ...systemWeights.regular,
    fontSize: 22,
    color: 'white'
  },
  inputField: {
    ...material.body2White,
    ...systemWeights.regular,
    fontSize: 18
  },
  helperText: {
    color: 'white',
    fontSize: 13,
    marginTop: '10%',
    marginLeft: '8%'
  },
  helperText2: {
    color: 'white',
    fontSize: 20,
    paddingTop: '5%'
  },
  modalText: {
    color: 'rgb(100,100,100)',
    fontSize: 18
  },
  modalTextSmall: {
    color: 'rgb(150,150,150)',
    fontSize: 13
  }
});

export default styles;
