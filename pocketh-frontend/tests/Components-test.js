/**
 * @format
 */

import 'react-native';
import React from 'react';


// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

jest.mock('../components/FormComponent', () => 'nothing');
jest.mock(
  '../node_modules/react-native-maps/lib/components/MapView',
  () => 'nothing'
);

it('CircularProgressComponent renders correctly', () => {
  renderer.create(<CircularProgressComponent />);
});
it('MapComponent renders correctly', () => {
  renderer.create(<MapComponent />);
});
it('ModalAddLocationComponent renders correctly', () => {
  renderer.create(<ModalAddLocationComponent />);
});
it('FormComponent renders correctly', () => {
  renderer.create(<FormComponent />);
});
it('LocationPreviewComponent renders correctly', () => {
  renderer.create(<LocationPreviewComponent />);
});
