/**
 * @format
 */

import 'react-native';
import React from 'react';
import 'react-navigation';


// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

jest.mock('../components/FormComponent', () => 'nothing');
jest.mock(
  '../node_modules/react-native-maps/lib/components/MapView',
  () => 'nothing'
);

it('IntroScreen renders correctly', () => {
  renderer.create(<IntroScreen />);
});
it('EnableNotificationsScreen renders correctly', () => {
  renderer.create(<EnableNotificationsScreen />);
});
it('EnableLocationScreen renders correctly', () => {
  renderer.create(<EnableLocationScreen />);
});
it('QRScannerScreen renders correctly', () => {
  renderer.create(<QRScannerScreen />);
});
it('TransactionHistoryScreen renders correctly', () => {
  renderer.create(<TransactionHistoryScreen />);
});
it('LocationScreen renders correctly', () => {
  renderer.create(<LocationScreen />);
});
it('HomeScreen renders correctly', () => {
  renderer.create(<HomeScreen />);
});
